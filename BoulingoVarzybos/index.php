<?php
	// nuskaitome konfigūracijų failą
	include 'config.php';

	// iškviečiame prisijungimo prie duomenų bazės klasę
	include 'utils/mysql.class.php';
	
	// nustatome pasirinktą modulį
	$module = '';
	if(isset($_GET['module'])) {
		$module = mysql::escape($_GET['module']);
	}
	
	// jeigu pasirinktas elementas (sutartis, automobilis ir kt.), nustatome elemento id
	$id = '';
	if(isset($_GET['id'])) {
		$id = mysql::escape($_GET['id']);
	}
	
	// nustatome, ar kuriamas naujas elementas
	$action = '';
	if(isset($_GET['action'])) {
		$action = mysql::escape($_GET['action']);
	}
	
	// jeigu šalinamas elementas, nustatome šalinamo elemento id
	$removeId = 0;
	if(!empty($_GET['remove'])) {
		// paruošiame $_GET masyvo id reikšmę SQL užklausai
		$removeId = mysql::escape($_GET['remove']);
	}
		
	// nustatome elementų sąrašo puslapio numerį
	$pageId = 1;
	if(!empty($_GET['page'])) {
		$pageId = mysql::escape($_GET['page']);
	}
	
	// nustatome, kiek įrašų rodysime elementų sąraše
	define('NUMBER_OF_ROWS_IN_PAGE', 10);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="robots" content="noindex">
		<title>Boulingo varzybos</title>
		<link rel="stylesheet" type="text/css" href="scripts/datetimepicker/jquery.datetimepicker.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="style/main.css" media="screen" />
		<script type="text/javascript" src="scripts/jquery-1.12.0.min.js"></script>
		<script type="text/javascript" src="scripts/datetimepicker/jquery.datetimepicker.full.min.js"></script>
		<script type="text/javascript" src="scripts/main.js"></script>
	</head>
	<body>
		<div id="body">
			<div id="header">
				<h3 id="slogan"><a href="index.php">Boulingo varžybos</a></h3>
			</div>
			<div id="content">
				<div id="topMenu">
					<ul class="float-left">
						<li><a href="index.php?module=boulingo_klubas" title="Boulingo_klubas"<?php if($module == 'boulingo_klubas') { echo 'class="active"'; } ?>>Boulingo_klubas</a></li>
						<li><a href="index.php?module=boulingo_kamuolys" title="Kamuoliai"<?php if($module == 'boulingo_kamuolys') { echo 'class="active"'; } ?>>Kamuoliai</a></li>
						<li><a href="index.php?module=darbuotojas" title="Darbuotojas"<?php if($module == 'darbuotojas') { echo 'class="active"'; } ?>>Darbuotojas</a></li>
						<li><a href="index.php?module=varzybos" title="Varžybos"<?php if($module == 'varzybos') { echo 'class="active"'; } ?>>Varžybos</a></li>
						<li><a href="index.php?module=mokejimas" title="Mokėjimai"<?php if($module == 'mokejimas') { echo 'class="active"'; } ?>>Mokėjimai</a></li>
						<li><a href="index.php?module=komanda" title="Komandos"<?php if($module == 'komanda') { echo 'class="active"'; } ?>>Komandos</a></li>
						<li><a href="index.php?module=tvarkarastis" title="Tvarkaraštis"<?php if($module == 'tvarkarastis') { echo 'class="active"'; } ?>>Tvarkaraštis</a></li>
					</ul>
					<ul class="float-right">
						<li><a href="index.php?module=report" title="Ataskaitos"<?php if($module == 'report') { echo 'class="active"'; } ?>>Ataskaitos</a></li>
					</ul>
				</div>
				<div id="contentMain">
					<?php
						if(!empty($module)) {
							if(empty($id) && empty($action)) {
								include "controls/{$module}_list.php";
							} else {
								include "controls/{$module}_edit.php";
							}
						}
					?>
					<div class="float-clear"></div>
				</div>
			</div>
			<div id="footer">

			</div>
		</div>
	</body>
</html>
