<?php

/**
 * Automobilių markių redagavimo klasė
 *
 * @author Jonas Mičiulis IF-4/14
 */

class strukturos {

	public function __construct() {
		
	}
	
	/**
	 * Markės išrinkimas
	 * @param type $id
	 * @return type
	 */
	 public function getStrukturos($id) {
		$query = "  SELECT *
					FROM `strukturos`
					WHERE `id_strukturos`='{$id}'";
		$data = mysql::select($query);
		
		return $data[0];
	}
	
	/**
	 * Markių sąrašo išrinkimas
	 * @param type $limit
	 * @param type $offset
	 * @return type
	 */
	public function getStukturosList($limit = null, $offset = null) {
		$limitOffsetString = "";
		if(isset($limit)) {
			$limitOffsetString .= " LIMIT {$limit}";
		}
		if(isset($offset)) {
			$limitOffsetString .= " OFFSET {$offset}";
		}
		
		$query = "  SELECT *
					FROM `strukturos`" . $limitOffsetString;
		$data = mysql::select($query);
		
		return $data;
	}

	/**
	 * Markių kiekio radimas
	 * @return type
	 */
	public function getStukturosListCount() {
		$query = "  SELECT COUNT(`id_strukturos`) as `kiekis`
					FROM `strukturos`";
		$data = mysql::select($query);
		
		return $data[0]['kiekis'];
	}
	
	/**
	 * Markės įrašymas
	 * @param type $data
	 */
	 

	/**
	 * Markės atnaujinimas
	 * @param type $data
	 */

	
	/**
	 * Markės šalinimas
	 * @param type $id
	 */

	/**
	 * Markės modelių kiekio radimas
	 * @param type $id
	 * @return type
	 */

	
	/**
	 * Didžiausiausios markės id reikšmės radimas
	 * @return type
	 */

	
}