<?php

/**
 * Boulingo klubo personalo redagavimo klasė
 *
 * @author Jonas Mičiulis IF-4/14
 */

class Personalas {

	public function __construct() {
		
	}
	
	/**
	 * Personalo išrinkimas
	 * @param type $id
	 * @return type
	 */
	 public function getPersonalas($id) {
		$query = "  SELECT *
					FROM `Personalas`
					WHERE `id_Personalas`='{$id}'";
		$data = mysql::select($query);
		
		return $data[0];
	}
	
	/**
	 * Personalo sąrašo išrinkimas
	 * @param type $limit
	 * @param type $offset
	 * @return type
	 */
/**	public function getPersonalasList($limit = null, $offset = null) {
		$limitOffsetString = "";
		if(isset($limit)) {
			$limitOffsetString .= " LIMIT {$limit}";
		}
		if(isset($offset)) {
			$limitOffsetString .= " OFFSET {$offset}";
		}
		
		$query = "  SELECT `Personalas`.`personalo_dydis`,
						   `Personalas`.`vadybininko_vardas`,
						   `Personalas`.`vadybininko_pavarde`,
						   `Personalas`.`id_Personalas`
					FROM `Personalas`
						LEFT JOIN `Boulingo_klubas`
							ON `Personalas`.`fk_Boulingo_klubasid_Boulingo_klubas`=`Boulingo_klubas`.`id_Boulingo_klubas` LIMIT {$limit} OFFSET {$offset}";
		$data = mysql::select($query);
		
		return $data;
	} */
	
	/**
	 * Personalo sąrašo išrinkimas
	 * @param type $limit
	 * @param type $offset
	 * @return type
	 */
	public function getPersonalasList($limit = null, $offset = null) {
		$limitOffsetString = "";
		if(isset($limit)) {
			$limitOffsetString .= " LIMIT {$limit}";
		}
		if(isset($offset)) {
			$limitOffsetString .= " OFFSET {$offset}";
		}
		
		$query = "  SELECT *
					FROM `Personalas`" . $limitOffsetString;
		$data = mysql::select($query);
		
		return $data;
	}

	/**
	 * Personalų kiekio radimas
	 * @return type
	 */
	public function getPersonalasListCount() {
		$query = "  SELECT COUNT(`id_Personalas`) as `kiekis`
					FROM `Personalas`";
		$data = mysql::select($query);
		
		return $data[0]['kiekis'];
	}
	
	/**
	 * Personalo įrašymas
	 * @param type $data
	 */
	 
	 public function insertPersonalas($data) {
		$query = "  INSERT INTO `Personalas`
								(
									`personalo_dydis`,
									`vadybininko_vardas`,
									`vadybininko_pavarde`,
									`fk_Boulingo_klubasid_Boulingo_klubas`,
									`id_Personalas`
								)
								VALUES
								(
									'{$data['personalo_dydis']}',
									'{$data['vadybininko_vardas']}',
									'{$data['vadybininko_pavarde']}',
									'{$data['fk_Boulingo_klubasid_Boulingo_klubas']}',
									'{$data['id_Personalas']}'
								)";
		mysql::query($query);
	}
	 

	/**
	 * Personalo atnaujinimas
	 * @param type $data
	 */
	 public function updatePersonalas($data) {
		$query = "  UPDATE `Personalas`
					SET    `personalo_dydis`='{$data['personalo_dydis']}',
						   `vadybininko_vardas`='{$data['vadybininko_vardas']}',
						   `vadybininko_pavarde`='{$data['vadybininko_pavarde']}',
						   `fk_Boulingo_klubasid_Boulingo_klubas`='{$data['fk_Boulingo_klubasid_Boulingo_klubas']}'
					WHERE `id_Personalas`='{$data['id_Personalas']}'";
		mysql::query($query);
	}
	
	/**
	 * Personalo šalinimas
	 * @param type $id
	 */
	 	public function deletePersonalas($id) {
		$query = "  DELETE FROM `Personalas`
					WHERE `id_PERSONALAS`='{$id}'";
		mysql::query($query);
	}
}