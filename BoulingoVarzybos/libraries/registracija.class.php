﻿<?php

/**
 * Boulingo varžybų registracijų klasė
 *
 * @author Jonas Mičiulis IF-4/14
 */

class Registracija {
	
	public function __construct() {
		
	}
	
	/**
	 * Registracijos išrinkimas
	 * @param type $id
	 * @return type
	 */
	 public function getRegistracija($id) {
		$query = "  SELECT *
					FROM `Registracija`
					WHERE `id_Registracija`='{$id}'";
		$data = mysql::select($query);
		
		return $data[0];
	}

	/**
	 * Registracijos sąrašo išrinkimas
	 * @param type $limit
	 * @param type $offset
	 * @return type
	 */
/**	public function getRegistracijaList($limit = null, $offset = null) {
		$limitOffsetString = "";
		if(isset($limit)) {
			$limitOffsetString .= " LIMIT {$limit}";
		}
		if(isset($offset)) {
			$limitOffsetString .= " OFFSET {$offset}";
		}
		
		$query = "  SELECT `Registracija`.`reg_pradzios_data`,
						   `Registracija`.`reg_pabaigos_data`,
						   `Registracija`.`min_komandu_sk`,
						   `Registracija`.`max_komandu_sk`,
						   `Registracija`.`startinis_mokestis`,
						   `Registracija`.`id_Registracija`
					FROM `Registracija`
						LEFT JOIN `Varzybos`
							ON `Registracija`.`fk_Varzybosid_Varzybos`=`Varzybos`.`id_Varzybos` LIMIT {$limit} OFFSET {$offset}";
		$data = mysql::select($query);
		
		return $data;
	} */
	
		public function getRegistracijaList($limit = null, $offset = null) {
		$limitOffsetString = "";
		if(isset($limit)) {
			$limitOffsetString .= " LIMIT {$limit}";
		}
		if(isset($offset)) {
			$limitOffsetString .= " OFFSET {$offset}";
		}
		
		$query = "  SELECT *
					FROM `Registracija`" . $limitOffsetString;
		$data = mysql::select($query);
		
		return $data;
	}

	/**
	 * Registracijų kiekio radimas
	 * @return type
	 */
	public function getRegistracijaListCount() {
		$query = "  SELECT COUNT(`Registracija`.`id_Registracija`) as `kiekis`
					FROM `Registracija`";
		$data = mysql::select($query);
		
		return $data[0]['kiekis'];
	}
	
	/**
	 * Registracijų išrinkimas pagal personalą
	 * @param type $brandId
	 * @return type
	 */
	 	public function getRegistracijaListByVarzybos($brandId) {
		$query = "  SELECT *
					FROM `Registracija`
					WHERE `id_Registracija`='{$brandId}'";
		$data = mysql::select($query);
		
		return $data;
	}
	
	/**
	 * Registracijos įrašymas
	 * @param type $data
	 */
	public function insertRegistracija($data) {
		$query = "  INSERT INTO `Registracija`
								(
									`reg_pradzios_data`,
									`reg_pabaigos_data`,
									`min_komandu_sk`,
									`max_komandu_sk`,
									`startinis_mokestis`,
									`fk_Varzybosid_Varzybos`,
									`id_Registracija`
								)
								VALUES
								(
									'{$data['reg_pradzios_data']}',
									'{$data['reg_pabaigos_data']}',
									'{$data['min_komandu_sk']}',
									'{$data['max_komandu_sk']}',
									'{$data['startinis_mokestis']}',
									'{$data['fk_Varzybosid_Varzybos']}',
									'{$data['id_Registracija']}'
								)";
		mysql::query($query);
	}
	
	/**
	 * Registracijos atnaujinimas
	 * @param type $data
	 */
	public function updateRegistracija($data) {
		$query = "  UPDATE `Registracija`
					SET    `reg_pradzios_data`='{$data['reg_pradzios_data']}',
					       `reg_pabaigos_data`='{$data['reg_pabaigos_data']}',
						   `min_komandu_sk`='{$data['min_komandu_sk']}',
						   `max_komandu_sk`='{$data['max_komandu_sk']}',
						   `startinis_mokestis`='{$data['startinis_mokestis']}',
						   `fk_Varzybosid_Varzybos`='{$data['fk_Varzybosid_Varzybos']}'
					WHERE `id_Registracija`='{$data['id_Registracija']}'";
		mysql::query($query);
	}

	
	/**
	 * Registracijos šalinimas
	 * @param type $id
	 */
	public function deleteRegistracija($id) {
		$query = "  DELETE FROM `Registracija`
					WHERE `id_Registracija`='{$id}'";
		mysql::query($query);
	}
	


	/**
	 * Didžiausios registracijos id reikšmės radimas
	 * @return type
	 */
	public function getMaxIdOfRegistracija() {
		$query = "  SELECT MAX(`id_Registracija`) AS `latestId`
					FROM `Registracija`";
		$data = mysql::select($query);
		
		return $data[0]['latestId'];
	}
	
}