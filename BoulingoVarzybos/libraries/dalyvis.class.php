<?php

/**
 * Boulingo varžybų dalyvių klasė
 *
 * @author Jonas Mičiulis IF-4/14
 */

class Dalyvis {
	
	public function __construct() {
		
	}
	
	
	public function getDalyvisListByID($id) {
		$limitOffsetString = "";
		if(isset($limit)) {
			$limitOffsetString .= " LIMIT {$limit}";
		}
		if(isset($offset)) {
			$limitOffsetString .= " OFFSET {$offset}";
		}
		
		$query = "  SELECT *
					FROM `Dalyvis`
					WHERE `fk_Komandaid_Komanda`='{$id}'
					" . $limitOffsetString;
		$data = mysql::select($query);
		
		return $data;
	}
	
	
	public function insertDalyvis($data) {
		foreach($data['id_Dalyviai'] as $key=>$val) {
				$query = "  INSERT INTO `Dalyvis`
										(
											`vardas`,
											`pavarde`,
											`individ_reitingas`,
											`vid_tasku_skaicius`,
											`rekordas`,
											`laimetu_turnyru_sk`,
											`id_Dalyvis`,
											`fk_Komandaid_Komanda`
										)
										VALUES
										(	
											'{$data['vardai'][$key]}',
											'{$data['pavardes'][$key]}',
											'{$data['individ_reitingai'][$key]}',
											'{$data['vid_tasku_skaiciai'][$key]}',
											'{$data['rekordai'][$key]}',
											'{$data['laimetu_turnyru_skaiciai'][$key]}',
											'{$data['id_Dalyviai'][$key]}',
											'{$data['id_Komanda']}'
										)";
				mysql::query($query);
		}
		
	}	
	
	public function deleteDalyvius($data) {
		foreach($data['id_Dalyviai'] as $key=>$val) {
			if($data['neaktyvus'] == array() || $data['neaktyvus'][$key] == 0) {
				$query = "  DELETE FROM `Dalyvis`
					WHERE `id_Dalyvis`='{$data['id_Dalyviai'][$key]}'";
				mysql::query($query);
			//	echo "{$data['id_SĄSKAITOS'][$key]} ";
			}
		}
	}
	
	public function deleteSas($id) {
		$query = "  DELETE FROM `Dalyvis`
			WHERE `id_Dalyvis`='{$id}'";
		mysql::query($query);
	}
	
		public function deleteDalyvisPrices($id) {
		$query = "  DELETE FROM `Dalyvis`
					WHERE `fk_Komandaid_Komanda`='{$id}'";
		mysql::query($query);
	}
	
	public function getDalyvius($dateFrom, $dateTo)
	{
		$query = "  SELECT *
				FROM `Dalyvis`
				WHERE `individ_reitingas`>='{$dateFrom}'
				AND `individ_reitingas`<='{$dateTo}'";
		$data = mysql::select($query);
		
		return $data;
		
	}
	
	public function getDalyviuz($dateFrom, $dateTo)
	{
		$query = "  SELECT `Dalyvis`.`vardas`,
						   `Dalyvis`.`pavarde`,
						   `Dalyvis`.`individ_reitingas`,
						   `Dalyvis`.`vid_tasku_skaicius`,
						   `Dalyvis`.`rekordas`,
						   `Dalyvis`.`laimetu_turnyru_sk`,
						   `Komanda`.`pavadinimas` as `pavad`
					FROM `Dalyvis`
						LEFT JOIN `Komanda`
							ON `Dalyvis`.`fk_Komandaid_Komanda`=`Komanda`.`id_Komanda`
					WHERE `individ_reitingas`>='{$dateFrom}'
					AND `individ_reitingas`<='{$dateTo}'
					ORDER by `individ_reitingas` DESC";
		$data = mysql::select($query);
		
		return $data;
		
	}
	
	public function getStatsOfDalyviai($dateFrom, $dateTo) {
		$whereClauseString = "";
		if(!empty($dateFrom)) {
			$whereClauseString .= " WHERE `Dalyvis`.`individ_reitingas`>='{$dateFrom}'";
			if(!empty($dateTo)) {
				$whereClauseString .= " AND `Dalyvis`.`individ_reitingas`<='{$dateTo}'";
			}
		} else {
			if(!empty($dateTo)) {
				$whereClauseString .= " WHERE `Dalyvis`.`individ_reitingas`<='{$dateTo}'";
			}
		}
		
		$query = "  SELECT MAX(`rekordas`) AS `wrekordas`
					FROM `Dalyvis`
						LEFT JOIN `Komanda`
							ON `Dalyvis`.`fk_Komandaid_Komanda`=`Komanda`.`id_Komanda`
					{$whereClauseString}";
		$data = mysql::select($query);

		return $data;
	}
	
}