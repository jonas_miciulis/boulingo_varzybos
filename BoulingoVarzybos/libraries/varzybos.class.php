<?php

/**
 * Boulingo varžybų redagavimo klasė
 *
 * @author Jonas Mičiulis IF-4/14
 */

class Varzybos {
	
	public function __construct() {
		
	}
	
	/**
	 * Varžybų išrinkimas
	 * @param type $id
	 * @return type
	 */
	 public function getVarzybos($id) {
		$query = "  SELECT *
					FROM `Varzybos`, `Registracija`
					WHERE `id_Varzybos`='{$id}' and `fk_Varzybosid_Varzybos`='{$id}' ";
		$data = mysql::select($query);
		
		return $data[0];
	}

	/**
	 * Varžybų sąrašo išrinkimas
	 * @param type $limit
	 * @param type $offset
	 * @return type
	 */
	public function getVarzybosList($limit = null, $offset = null) {
		$limitOffsetString = "";
		if(isset($limit)) {
			$limitOffsetString .= " LIMIT {$limit}";
		}
		if(isset($offset)) {
			$limitOffsetString .= " OFFSET {$offset}";
		}
		
		$query = "  SELECT `Varzybos`.`pavadinimas`,
						   `Varzybos`.`varzybų_pradzia`,
						   `Varzybos`.`varzybų_pabaiga`,
						   `Varzybos`.`organizatoriu_sk`,
						   `Varzybos`.`prizinis_fondas`,
						   `Varzybos`.`pagrindiniai_remejai`,
						   `Varzybos`.`prizininku_sk`,
						   `Varzybos`.`id_Varzybos`,
						   `strukturos`.`name` AS `struktura`
					FROM `Varzybos`
						LEFT JOIN `strukturos`
							ON `Varzybos`.`struktura`=`strukturos`.`id_strukturos`" . $limitOffsetString;
		$data = mysql::select($query);
		
		return $data;
	}

	/**
	 * Varžybų kiekio radimas
	 * @return type
	 */
	public function getVarzybosListCount() {
		$query = "  SELECT COUNT(`Varzybos`.`id_Varzybos`) as `kiekis`
					FROM `Varzybos`";
		$data = mysql::select($query);
		
		return $data[0]['kiekis'];
	}
	
	/**
	 * Varžybų išrinkimas pagal personalą
	 * @param type $brandId
	 * @return type
	 */
/**	 	public function getModelListByBrand($brandId) {
		$query = "  SELECT *
					FROM `Varzybos`
					WHERE `id_Varzybos`='{$brandId}'";
		$data = mysql::select($query);
		
		return $data;
	} */

	/**
	 * Varžybų atnaujinimas
	 * @param type $data
	 */
	public function updateVarzybos($data) {
		$query = "  UPDATE `Varzybos`, `Registracija`
					SET    `pavadinimas`='{$data['pavadinimas']}',
						   `varzybų_pradzia`='{$data['varzybų_pradzia']}',
					       `varzybų_pabaiga`='{$data['varzybų_pabaiga']}',
						   `organizatoriu_sk`='{$data['organizatoriu_sk']}',
						   `prizinis_fondas`='{$data['prizinis_fondas']}',
						   `pagrindiniai_remejai`='{$data['pagrindiniai_remejai']}',
						   `prizininku_sk`='{$data['prizininku_sk']}',
						   `struktura`='{$data['struktura']}',
						   
						   `reg_pradzios_data`='{$data['reg_pradzios_data']}',
					       `reg_pabaigos_data`='{$data['reg_pabaigos_data']}',
						   `min_komandu_sk`='{$data['min_komandu_sk']}',
						   `max_komandu_sk`='{$data['max_komandu_sk']}',
						   `startinis_mokestis`='{$data['startinis_mokestis']}'
					WHERE `id_Varzybos`='{$data['id_Varzybos']}' and `fk_Varzybosid_Varzybos`='{$data['id_Varzybos']}' ";
		mysql::query($query);
	}
	/**
	 * Varžybų įrašymas
	 * @param type $data
	 */
	public function insertVarzybos($data) {
		$query = "  INSERT INTO `Varzybos`
								(
									`pavadinimas`,
									`varzybų_pradzia`,
									`varzybų_pabaiga`,
									`organizatoriu_sk`,
									`prizinis_fondas`,
									`pagrindiniai_remejai`,
									`prizininku_sk`,
									`struktura`,
									`id_Varzybos`
								)
								VALUES
								(
									'{$data['pavadinimas']}',
									'{$data['varzybų_pradzia']}',
									'{$data['varzybų_pabaiga']}',
									'{$data['organizatoriu_sk']}',
									'{$data['prizinis_fondas']}',
									'{$data['pagrindiniai_remejai']}',
									'{$data['prizininku_sk']}',
									'{$data['struktura']}',
									'{$data['id_Varzybos']}'
								)";
								
		$query2 = "  INSERT INTO `Registracija`
								(
									`reg_pradzios_data`,
									`reg_pabaigos_data`,
									`min_komandu_sk`,
									`max_komandu_sk`,
									`startinis_mokestis`,
									`fk_Varzybosid_Varzybos`,
								)
								VALUES
								(
									'{$data['reg_pradzios_data']}',
									'{$data['reg_pabaigos_data']}',
									'{$data['min_komandu_sk']}',
									'{$data['max_komandu_sk']}',
									'{$data['startinis_mokestis']}',
									'{$data['id_Varzybos']}'
								)";
								
		mysql::query($query);
		mysql::query($query2);
	}
	
	/**
	 * Varžybų šalinimas
	 * @param type $id
	 */
	public function deleteVarzybos($id) {
		$query = "  DELETE FROM `Varzybos`
					WHERE `id_Varzybos`='{$id}'";
		mysql::query($query);
	}
	
	

	
}