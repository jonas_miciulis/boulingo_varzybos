﻿<?php

/**
 * Boulingo klubo darbuotojų redagavimo klasė
 *
 * @author Jonas Mičiulis IF-4/14
 */

class Darbuotojas {
	
	public function __construct() {
		
	}
	
	/**
	 * Darbuotojo išrinkimas
	 * @param type $id
	 * @return type
	 */
	 public function getDarbuotojas($id) {
		$query = "  SELECT *
					FROM `Darbuotojas`
					WHERE `id_Darbuotojas`='{$id}'";
		$data = mysql::select($query);
		
		return $data[0];
	}

	/**
	 * Darbuotojų sąrašo išrinkimas
	 * @param type $limit
	 * @param type $offset
	 * @return type
	 */
	public function getDarbuotojasList($limit = null, $offset = null) {
		$limitOffsetString = "";
		if(isset($limit)) {
			$limitOffsetString .= " LIMIT {$limit}";
		}
		if(isset($offset)) {
			$limitOffsetString .= " OFFSET {$offset}";
		}
		
		$query = "  SELECT `Darbuotojas`.`id_Darbuotojas`,
						   `Darbuotojas`.`vardas`,
						   `Darbuotojas`.`pavarde`
					FROM `Darbuotojas`
						LEFT JOIN `Personalas`
							ON `Darbuotojas`.`fk_Personalasid_Personalas`=`Personalas`.`id_Personalas`" . $limitOffsetString;
		$data = mysql::select($query);
		
		return $data;
	}

	/**
	 * Darbuotojų kiekio radimas
	 * @return type
	 */
	public function getDarbuotojasListCount() {
		$query = "  SELECT COUNT(`Darbuotojas`.`id_Darbuotojas`) as `kiekis`
					FROM `Darbuotojas`
						LEFT JOIN `Personalas`
							ON `Darbuotojas`.`fk_Personalasid_Personalas`=`Personalas`.`id_Personalas`
							";
		$data = mysql::select($query);
		
		return $data[0]['kiekis'];
	}
	
	/**
	 * Darbuotojų išrinkimas pagal personalą
	 * @param type $PersonalasId
	 * @return type
	 */
	 	public function getDarbuotojasListByPersonalas($PersonalasId) {
		$query = "  SELECT *
					FROM `Darbuotojas`
					WHERE `fk_Personalasid_Personalas`='{$PersonalasId}'";
		$data = mysql::select($query);
		
		return $data;
	}

	/**
	 * Darbuotojo atnaujinimas
	 * @param type $data
	 */
	public function updateDarbuotojas($data) {
		$query = "  UPDATE `Darbuotojas`
					SET	   `vardas`='{$data['vardas']}',
						   `pavarde`='{$data['pavarde']}',
						   `fk_Personalasid_Personalas`='{$data['fk_Personalasid_Personalas']}'
					WHERE `id_Darbuotojas`='{$data['id_Darbuotojas']}'";
		mysql::query($query);
	}
	/**
	 * Darbuotojo įrašymas
	 * @param type $data
	 */
	public function insertDarbuotojas($data) {
		$query = "  INSERT INTO `Darbuotojas`
								(
									`vardas`,
									`pavarde`,
									`id_Darbuotojas`,
									`fk_Personalasid_Personalas`
								)
								VALUES
								(
									'{$data['vardas']}',
									'{$data['pavarde']}',
									'{$data['id_Darbuotojas']}',
									'{$data['fk_Personalasid_Personalas']}'
								)";
		mysql::query($query);
	}
	
	/**
	 * Darbuotojo šalinimas
	 * @param type $id
	 */
	public function deleteDarbuotojas($id) {
		$query = "  DELETE FROM `Darbuotojas`
					WHERE `id_Darbuotojas`='{$id}'";
		mysql::query($query);
	}
	
	/**
	 * Didžiausios Darbuotojo id reikšmės radimas
	 * @return type
	 */
	public function getMaxIdOfDarbuotojas() {
		$query = "  SELECT MAX(`id_Darbuotojas`) AS `latestId`
					FROM `Darbuotojas`";
		$data = mysql::select($query);
		
		return $data[0]['latestId'];
	}
	
}