<?php

/**
 * Boulingo komandų redagavimo klasė
 *
 * @author Jonas Mičiulis IF-4/14
 */

class Komanda {

	public function __construct() {
		
	}
	
	/**
	 * Komandos išrinkimas
	 * @param type $id
	 * @return type
	 */
	public function getKomanda($id) {
		$query = "  SELECT *
					FROM `Komanda`
					WHERE `id_Komanda`='{$id}'";
		$data = mysql::select($query);
		
		return $data[0];
	}
	
	/**
	 * Komandų sąrašo išrinkimas
	 * @param type $limit
	 * @param type $offset
	 * @return type
	 */
	public function getKomandaList($limit = null, $offset = null) {
		$limitOffsetString = "";
		if(isset($limit)) {
			$limitOffsetString .= " LIMIT {$limit}";
		}
		if(isset($offset)) {
			$limitOffsetString .= " OFFSET {$offset}";
		}
		
		$query = "  SELECT *
					FROM `Komanda`" . $limitOffsetString;
		$data = mysql::select($query);
		
		return $data;
	}

	/**
	 * Komandų kiekio radimas
	 * @return type
	 */
	public function getKomandaListCount() {
		$query = "  SELECT COUNT(`id_Komanda`) as `kiekis`
					FROM `Komanda`";
		$data = mysql::select($query);
		
		return $data[0]['kiekis'];
	}
	
	/**
	 * Komandos įrašymas
	 * @param type $data
	 */
	public function insertKomanda($data) {
		$query = "  INSERT INTO `Komanda`
								(
									`id_Komanda`,
									`pavadinimas`,
									`komandos_nariu_skaicius`,
									`reitingas`,
									`laimetu_turnyru_sk`,
									`komandos_telefono_nr`
								)
								VALUES
								(
									'{$data['id_Komanda']}',
									'{$data['pavadinimas']}',
									'{$data['komandos_nariu_skaicius']}',
									'{$data['reitingas']}',
									'{$data['laimetu_turnyru_sk']}',
									'{$data['komandos_telefono_nr']}'
								)";
		mysql::query($query);
	}
	
	/**
	 * Komandos atnaujinimas
	 * @param type $data
	 */
	public function updateKomanda($data) {
		$query = "  UPDATE `Komanda`
					SET    `pavadinimas`='{$data['pavadinimas']}',
						   `komandos_nariu_skaicius`='{$data['komandos_nariu_skaicius']}',
						   `reitingas`='{$data['reitingas']}',
						   `laimetu_turnyru_sk`='{$data['laimetu_turnyru_sk']}',
						   `komandos_telefono_nr`='{$data['komandos_telefono_nr']}'
					WHERE `id_Komanda`='{$data['id_Komanda']}'";
		mysql::query($query);
	}
	
	/**
	 * Markės šalinimas
	 * @param type $id
	 */
	public function deleteKomanda($id) {
		$query = "  DELETE FROM `Komanda`
					WHERE `id_Komanda`='{$id}'";
		mysql::query($query);
	}
	
	/**
	 * Markės modelių kiekio radimas
	 * @param type $id
	 * @return type
	 */
/**	public function getModelCountOfBrand($id) {
		$query = "  SELECT COUNT(`modeliai`.`id`) AS `kiekis`
					FROM `markes`
						INNER JOIN `modeliai`
							ON `markes`.`id`=`modeliai`.`fk_marke`
					WHERE `markes`.`id`='{$id}'";
		$data = mysql::select($query);
		
		return $data[0]['kiekis'];
	} */
	
	/**
	 * Didžiausiausios Komanda id reikšmės radimas
	 * @return type
	 */
	public function getMaxIdOfKomanda() {
		$query = "  SELECT MAX(`id_Komanda`) AS `latestId`
					FROM `Komanda`";
		$data = mysql::select($query);
		
		return $data[0]['latestId'];
	}
	
	public function getKomandos() {
		$query = "  SELECT *
					FROM `Komanda`";
		$data = mysql::select($query);
		
		return $data;
	}
	
	public function getKomandas($dateFrom, $dateTo)
	{
		$query = "  SELECT  `pavadinimas`,
							`komandos_nariu_skaicius`,
							`reitingas`,
							`laimetu_turnyru_sk`
					FROM `Komanda`
					WHERE `reitingas`>='{$dateFrom}'
					AND `reitingas`<='{$dateTo}'
					ORDER by `reitingas` DESC";
		$data = mysql::select($query);
		
		return $data;
		
	}
	
	public function getKomandosStats($dateFrom, $dateTo)
	{
		$query = "  SELECT  SUM(`komandos_nariu_skaicius`) AS `kiekis`
					FROM `Komanda`
					WHERE `reitingas`>='{$dateFrom}'
					AND `reitingas`<='{$dateTo}'";
		$data = mysql::select($query);
		
		return $data;
		
	}
	
	public function getNelaimejusios($dateFrom, $dateTo)
	{
		$query = "  SELECT COUNT(`id_Komanda`) AS `kiekis2`
					FROM `Komanda`
					WHERE `reitingas`>='{$dateFrom}'
					AND `reitingas`<='{$dateTo}'
					AND `laimetu_turnyru_sk`<1";
		$data = mysql::select($query);
		
		return $data;
		
	}
	
}