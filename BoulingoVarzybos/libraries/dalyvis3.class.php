<?php

/**
 * Boulingo varžybų tvarkaraščių klasė
 *
 * @author Jonas Mičiulis IF-4/14
 */

class Dalyvis {
	
	public function __construct() {
		
	}
	
	/**
	 * Tvarkaraščio išrinkimas
	 * @param type $id
	 * @return type
	 */
	 public function getDalyvis($id) {
		$query = "  SELECT *
					FROM `Dalyvis`
					WHERE `id_Dalyvis`='{$id}'";
		$data = mysql::select($query);
		
		return $data[0];
	}

	/**
	 * Tvarkaraščio sąrašo išrinkimas
	 * @param type $limit
	 * @param type $offset
	 * @return type
	 */
	public function getDalyvisList($limit = null, $offset = null) {
		$limitOffsetString = "";
		if(isset($limit)) {
			$limitOffsetString .= " LIMIT {$limit}";
		}
		if(isset($offset)) {
			$limitOffsetString .= " OFFSET {$offset}";
		}
		
		$query = "  SELECT `Dalyvis`.`vardas`,
						   `Dalyvis`.`pavarde`,
						   `Dalyvis`.`individ_reitingas`,
						   `Dalyvis`.`vid_tasku_skaicius`,
						   `Dalyvis`.`rekordas`,
						   `Dalyvis`.`laimetu_turnyru_sk`,
						   `Dalyvis`.`id_Dalyvis`
					FROM `Dalyvis`
						LEFT JOIN `Komanda`
							ON `Dalyvis`.`fk_Komandaid_Komanda`=`Komanda`.`id_Komanda` LIMIT {$limit} OFFSET {$offset}";
		$data = mysql::select($query);
		
		return $data;
	}

	/**
	 * Tvarkaraščio kiekio radimas
	 * @return type
	 */
	public function getDalyvisListCount() {
		$query = "  SELECT COUNT(`Dalyvis`.`id_Dalyvis`) as `kiekis`
					FROM `Dalyvis`";
		$data = mysql::select($query);
		
		return $data[0]['kiekis'];
	}
	
	/**
	 * Tvarkaraščio išrinkimas pagal personalą
	 * @param type $brandId
	 * @return type
	 */
	 	public function getDalyvisListByKomanda($brandId) {
		$query = "  SELECT *
					FROM `Dalyvis`
					WHERE `id_Dalyvis`='{$brandId}'";
		$data = mysql::select($query);
		
		return $data;
	}

	/**
	 * Tvarkaraščio atnaujinimas
	 * @param type $data
	 */
	public function updateDalyvis($data) {
		$query = "  UPDATE `Dalyvis`
					SET    `vardas`='{$data['vardas']}',
					       `pavarde`='{$data['pavarde']}',
						   `individ_reitingas`='{$data['individ_reitingas']}',
						   `vid_tasku_skaicius`='{$data['vid_tasku_skaicius']}',
						   `rekordas`='{$data['rekordas']}',
						   `laimetu_turnyru_sk`='{$data['laimetu_turnyru_sk']}',
						   `fk_Komandaid_Komanda`='{$data['fk_Komandaid_Komanda']}'
					WHERE `id_Dalyvis`='{$data['id_Dalyvis']}'";
		mysql::query($query);
	}
	/**
	 * Tvarkaraščio įrašymas
	 * @param type $data
	 */
	public function insertDalyvis($data) {
		$query = "  INSERT INTO `Dalyvis`
								(
									`vardas`,
									`pavarde`,
									`individ_reitingas`,
									`vid_tasku_skaicius`,
									`rekordas`,
									`laimetu_turnyru_sk`,
									`fk_Komandaid_Komanda`,
									`id_Dalyvis`
								)
								VALUES
								(
									'{$data['vardas']}',
									'{$data['pavarde']}',
									'{$data['individ_reitingas']}',
									'{$data['vid_tasku_skaicius']}',
									'{$data['rekordas']}',
									'{$data['laimetu_turnyru_sk']}',
									'{$data['fk_Komandaid_Komanda']}',
									'{$data['id_Dalyvis']}'
								)";
		mysql::query($query);
	}
	
	/**
	 * Tvarkaraščio šalinimas
	 * @param type $id
	 */
	public function deleteDalyvis($id) {
		$query = "  DELETE FROM `Dalyvis`
					WHERE `id_Dalyvis`='{$id}'";
		mysql::query($query);
	}
	
	/**
	 * Nurodyto Tvarkaraščio kiekio radimas
	 * @param type $id
	 * @return type
	 */

	
	/**
	 * Didžiausios Tvarkaraščio id reikšmės radimas
	 * @return type
	 */
	public function getMaxIdOfDalyvis() {
		$query = "  SELECT MAX(`id_Dalyvis`) AS `latestId`
					FROM `Dalyvis`";
		$data = mysql::select($query);
		
		return $data[0]['latestId'];
	}
	
}