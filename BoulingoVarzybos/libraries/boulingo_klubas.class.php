﻿<?php

/**
 * Boulingo klubų redagavimo klasė
 *
 * @author Jonas Mičiulis IF-4/14
 */

class Boulingo_klubas {

	public function __construct() {
		
	}
	
	/**
	 * Boulingo klubo išrinkimas
	 * @param type $id
	 * @return type
	 */
	 public function getBoulingoKlubas($id) {
		$query = "  SELECT *
					FROM `Boulingo_klubas`, `Kontaktai`, `Personalas`
					WHERE `id_Boulingo_klubas`='{$id}' and `fk_Boulingo_klubasid_Boulingo_klubas`='{$id}' and `fk_Boulingo_klubasid`='{$id}' ";
		$data = mysql::select($query);
		
		return $data[0];
	}
	
	/**
	 * Boulingo klubų sąrašo išrinkimas
	 * @param type $limit
	 * @param type $offset
	 * @return type
	 */
	public function getBoulingoKlubasList($limit = null, $offset = null) {
		$limitOffsetString = "";
		if(isset($limit)) {
			$limitOffsetString .= " LIMIT {$limit}";
		}
		if(isset($offset)) {
			$limitOffsetString .= " OFFSET {$offset}";
		}
		
		$query = "  SELECT *
					FROM `Boulingo_klubas`" . $limitOffsetString;
		$data = mysql::select($query);
		
		return $data;
	}

	/**
	 * Boulingo klubo kiekio radimas
	 * @return type
	 */
	public function getBoulingoKlubasListCount() {
		$query = "  SELECT COUNT(`id_Boulingo_klubas`) as `kiekis`
					FROM `Boulingo_klubas`";
		$data = mysql::select($query);
		
		return $data[0]['kiekis'];
	}
	
	/**
	 * Boulingo klubo įrašymas
	 * @param type $data
	 */
	 public function insertBoulingoKlubas($data) {
		$query1 = "  INSERT INTO `Boulingo_klubas`
								(
									`id_Boulingo_klubas`,
									`pavadinimas`,
									`taku_sk`,
									`takelio_kaina_valandai`
								)
								VALUES
								(
									'{$data['id_Boulingo_klubas']}',
									'{$data['pavadinimas']}',
									'{$data['taku_sk']}',
									'{$data['takelio_kaina_valandai']}'
								)";
								
		$query2 = "  INSERT INTO `Kontaktai`
								(
									`telefono_numeris`,
									`elektroninis_pastas`,
									`adresas`,
									`fk_Boulingo_klubasid_Boulingo_klubas`
								)
								VALUES
								(
									'{$data['telefono_numeris']}',
									'{$data['elektroninis_pastas']}',
									'{$data['adresas']}',
									'{$data['id_Boulingo_klubas']}'
								)";	
		$query3 = "  INSERT INTO `Personalas`
								(
									`personalo_dydis`,
									`vadybininko_vardas`,
									`vadybininko_pavarde`,
									`id_Personalas`,
									`fk_Boulingo_klubasid`
								)
								VALUES
								(
									'{$data['personalo_dydis']}',
									'{$data['vadybininko_vardas']}',
									'{$data['vadybininko_pavarde']}',
									'{$data['id_Personalas']}',
									'{$data['id_Boulingo_klubas']}'
								)";
								
		mysql::query($query1);
		mysql::query($query2);
		mysql::query($query3);
	}
	
	/**
	 * Boulingo klubo atnaujinimas
	 * @param type $data
	 */
	 public function updateBoulingoKlubas($data) {
		$query = "  UPDATE `Boulingo_klubas`, `Kontaktai`
					SET    `pavadinimas`='{$data['pavadinimas']}',
					       `taku_sk`='{$data['taku_sk']}',
						   `takelio_kaina_valandai`='{$data['takelio_kaina_valandai']}',
						   
						   `telefono_numeris`='{$data['telefono_numeris']}',
						   `elektroninis_pastas`='{$data['elektroninis_pastas']}',
						   `adresas`='{$data['adresas']}',
						   
						   `personalo_dydis`='{$data['personalo_dydis']}',
						   `vadybininko_vardas`='{$data['vadybininko_vardas']}',
						   `vadybininko_pavarde`='{$data['vadybininko_pavarde']}'
					WHERE `id_Boulingo_klubas`='{$data['id_Boulingo_klubas']}' and `fk_Boulingo_klubasid_Boulingo_klubas`='{$data['id_Boulingo_klubas']}' and `fk_Boulingo_klubasid`='{$data['id_Boulingo_klubas']}' ";
		mysql::query($query);
	}
	
	/**
	 * Boulingo klubo šalinimas
	 * @param type $id
	 */
	public function deleteBoulingoKlubas($id) {	
		$query1 = "  DELETE FROM `Kontaktai`
					WHERE `fk_Boulingo_klubasid_Boulingo_klubas`='{$id}'";
		$query2 = "  DELETE FROM `Boulingo_klubas`
					WHERE `id_Boulingo_klubas`='{$id}'";
		$query3 = "  DELETE FROM `Personalas`
					WHERE `fk_Boulingo_klubasid`='{$id}'";
		
		mysql::query($query1);
		mysql::query($query2);
		mysql::query($query3);
	}
	
	/**
	 * Boulingo klubų kiekio radimas
	 * @param type $id
	 * @return type
	 */
	public function getBoulingoKamuolysCountOfBoulingoKlubas($id) {
		$query = "  SELECT COUNT(`Boulingo_kamuolys`.`id_Boulingo_kamuolys`) AS `kiekis`
					FROM `Boulingo_klubas`
						INNER JOIN `Boulingo_kamuolys`
							ON `Boulingo_klubas`.`id_Boulingo_klubas`=`Boulingo_kamuolys`.`fk_Boulingo_klubasid_Boulingo_klubas`
					WHERE `Boulingo_klubas`.`id_Boulingo_klubas`='{$id}'";
		$data = mysql::select($query);
		
		return $data[0]['kiekis'];
	}

	
	/**
	 * Didžiausiausios boulingo klubo id reikšmės radimas
	 * @return type
	 */
	 	public function getMaxIdOfBoulingoKlubas() {
		$query = "  SELECT MAX(`id_Boulingo_klubas`) AS `latestId`
					FROM `Boulingo_klubas`";
		$data = mysql::select($query);
		
		return $data[0]['latestId'];
	}
	
}