<?php

/**
 * Boulingo klubų parduodamų kamuolių klasė
 *
 * @author Jonas Mičiulis IF-4/14
 */

class Boulingo_kamuolys {
	
	public function __construct() {
		
	}
	
	/**
	 * boulingo kamuolio išrinkimas
	 * @param type $id
	 * @return type
	 */
	 public function getBoulingoKamuolys($id) {
		$query = "  SELECT *
					FROM `Boulingo_kamuolys`
					WHERE `id_Boulingo_kamuolys`='{$id}'";
		$data = mysql::select($query);
		
		return $data[0];
	}

	/**
	 * boulingo kamuolio sąrašo išrinkimas
	 * @param type $limit
	 * @param type $offset
	 * @return type
	 */
	public function getBoulingoKamuolysList($limit = null, $offset = null) {
		$limitOffsetString = "";
		if(isset($limit)) {
			$limitOffsetString .= " LIMIT {$limit}";
		}
		if(isset($offset)) {
			$limitOffsetString .= " OFFSET {$offset}";
		}
		
		$query = "  SELECT `Boulingo_kamuolys`.`kaina`,
						   `svoriai`.`name` AS `svoris_svarais`,
						   `Boulingo_kamuolys`.`id_Boulingo_kamuolys`
					FROM `Boulingo_kamuolys`
						LEFT JOIN `Boulingo_klubas`
							ON `Boulingo_kamuolys`.`fk_Boulingo_klubasid_Boulingo_klubas`=`Boulingo_klubas`.`id_Boulingo_klubas`
						LEFT JOIN `svoriai`
							ON `Boulingo_kamuolys`.`svoris_svarais`=`svoriai`.`id_svoriai`" . $limitOffsetString;
		$data = mysql::select($query);
		
		return $data;
	}

	/**
	 * boulingo kamuolio kiekio radimas
	 * @return type
	 */
	public function getBoulingoKamuolysListCount() {
		$query = "  SELECT COUNT(`Boulingo_kamuolys`.`id_Boulingo_kamuolys`) as `kiekis`
					FROM `Boulingo_kamuolys`";
		$data = mysql::select($query);
		
		return $data[0]['kiekis'];
	}
	
	/**
	 * boulingo kamuolio išrinkimas pagal personalą
	 * @param type $brandId
	 * @return type
	 */
	 	public function getBoulingoKamuolysListByBoulingoKlubas($brandId) {
		$query = "  SELECT *
					FROM `Boulingo_kamuolys`
					WHERE `id_Boulingo_kamuolys`='{$brandId}'";
		$data = mysql::select($query);
		
		return $data;
	}

	/**
	 * boulingo kamuolio atnaujinimas
	 * @param type $data
	 */
	public function updateBoulingoKamuolys($data) {
		$query = "  UPDATE `Boulingo_kamuolys`
					SET    `kaina`='{$data['kaina']}',
					       `svoris_svarais`='{$data['svoris_svarais']}',
						   `fk_Boulingo_klubasid_Boulingo_klubas`='{$data['fk_Boulingo_klubasid_Boulingo_klubas']}'
					WHERE `id_Boulingo_kamuolys`='{$data['id_Boulingo_kamuolys']}'";
		mysql::query($query);
	}
	/**
	 * boulingo kamuolio įrašymas
	 * @param type $data
	 */
	public function insertBoulingoKamuolys($data) {
		$query = "  INSERT INTO `Boulingo_kamuolys`
								(
									`kaina`,
									`svoris_svarais`,
									`fk_Boulingo_klubasid_Boulingo_klubas`,
									`id_Boulingo_kamuolys`
								)
								VALUES
								(
									'{$data['kaina']}',
									'{$data['svoris_svarais']}',
									'{$data['fk_Boulingo_klubasid_Boulingo_klubas']}',
									'{$data['id_Boulingo_kamuolys']}'
								)";
		mysql::query($query);
	}
	
	/**
	 * boulingo kamuolio šalinimas
	 * @param type $id
	 */
	public function deleteBoulingoKamuolys($id) {
		$query = "  DELETE FROM `Boulingo_kamuolys`
					WHERE `id_Boulingo_kamuolys`='{$id}'";
		mysql::query($query);
	}
	
	/**
	 * Nurodyto boulingo kamuolio kiekio radimas
	 * @param type $id
	 * @return type
	 */

	
	/**
	 * Didžiausios boulingo kamuolio id reikšmės radimas
	 * @return type
	 */
	public function getMaxIdOfBoulingoKamuolys() {
		$query = "  SELECT MAX(`id_Boulingo_kamuolys`) AS `latestId`
					FROM `Boulingo_kamuolys`";
		$data = mysql::select($query);
		
		return $data[0]['latestId'];
	}
	
	public function getKamuolius($dateFrom, $dateTo)
	{
		$query = "  SELECT `Boulingo_kamuolys`.`kaina`,
						   `svoriai`.`name` AS `svoris_svarais`,
						   `Boulingo_kamuolys`.`id_Boulingo_kamuolys`,
						   `Boulingo_klubas`.`pavadinimas` as `klubas`,
						   `Kontaktai`.`telefono_numeris` as `telefonas`
					FROM `Boulingo_kamuolys`
						LEFT JOIN `Boulingo_klubas`
							ON `Boulingo_kamuolys`.`fk_Boulingo_klubasid_Boulingo_klubas`=`Boulingo_klubas`.`id_Boulingo_klubas`
						LEFT JOIN `Kontaktai`
							ON `Boulingo_klubas`.`id_Boulingo_klubas`=`Kontaktai`.`fk_Boulingo_klubasid_Boulingo_klubas`
						LEFT JOIN `svoriai`
							ON `Boulingo_kamuolys`.`svoris_svarais`=`svoriai`.`id_svoriai`
					WHERE `kaina`>='{$dateFrom}'
					AND `kaina`<='{$dateTo}'
					ORDER by `klubas` ASC";
		$data = mysql::select($query);
		
		return $data;
		
	}
	
	public function getMin($dateFrom, $dateTo) {
		$whereClauseString = "";
		if(!empty($dateFrom)) {
			$whereClauseString .= " WHERE `Boulingo_kamuolys`.`kaina`>='{$dateFrom}'";
			if(!empty($dateTo)) {
				$whereClauseString .= " AND `Boulingo_kamuolys`.`kaina`<='{$dateTo}'";
			}
		} else {
			if(!empty($dateTo)) {
				$whereClauseString .= " WHERE `Boulingo_kamuolys`.`kaina`<='{$dateTo}'";
			}
		}
		
		$query = "  SELECT MIN(`kaina`) AS `min`
					FROM `Boulingo_kamuolys`
						LEFT JOIN `Boulingo_klubas`
							ON `Boulingo_kamuolys`.`fk_Boulingo_klubasid_Boulingo_klubas`=`Boulingo_klubas`.`id_Boulingo_klubas`
						LEFT JOIN `svoriai`
							ON `Boulingo_kamuolys`.`svoris_svarais`=`svoriai`.`id_svoriai` {$whereClauseString}";
		$data = mysql::select($query);

		return $data;
	}
	
}