<?php

/**
 * Boulingo varžybų tvarkaraščių klasė
 *
 * @author Jonas Mičiulis IF-4/14
 */

class Mokejimas {
	
	public function __construct() {
		
	}
	
	/**
	 * Tvarkaraščio išrinkimas
	 * @param type $id
	 * @return type
	 */
	 public function getMokejimas($id) {
		$query = "  SELECT *
					FROM `Mokejimas`
					WHERE `id_Mokejimas`='{$id}'";
		$data = mysql::select($query);
		
		return $data[0];
	}

	/**
	 * Tvarkaraščio sąrašo išrinkimas
	 * @param type $limit
	 * @param type $offset
	 * @return type
	 */
	public function getMokejimasList($limit = null, $offset = null) {
		$limitOffsetString = "";
		if(isset($limit)) {
			$limitOffsetString .= " LIMIT {$limit}";
		}
		if(isset($offset)) {
			$limitOffsetString .= " OFFSET {$offset}";
		}
		
		$query = "  SELECT `Mokejimas`.`data`,
						   `Mokejimas`.`pervedama_suma`,
						   `Mokejimas`.`id_Mokejimas`
					FROM `Mokejimas`
						LEFT JOIN `Komanda`
							ON `Mokejimas`.`fk_Komandaid_Komanda`=`Komanda`.`id_Komanda`
						LEFT JOIN `Registracija`
							ON `Mokejimas`.`fk_Registracijaid_Registracija`=`Registracija`.`id_Registracija`" . $limitOffsetString;
		$data = mysql::select($query);
		
		return $data;
	}

	/**
	 * Tvarkaraščio kiekio radimas
	 * @return type
	 */
	public function getMokejimasListCount() {
		$query = "  SELECT COUNT(`Mokejimas`.`id_Mokejimas`) as `kiekis`
					FROM `Mokejimas`";
		$data = mysql::select($query);
		
		return $data[0]['kiekis'];
	}
	
	/**
	 * Tvarkaraščio išrinkimas pagal personalą
	 * @param type $brandId
	 * @return type
	 */
	 	public function getMokejimasListByRegistracija($brandId) {
		$query = "  SELECT *
					FROM `Mokejimas`
					WHERE `id_Mokejimas`='{$brandId}'";
		$data = mysql::select($query);
		
		return $data;
	}

	/**
	 * Tvarkaraščio atnaujinimas
	 * @param type $data
	 */
	public function updateMokejimas($data) {
		$query = "  UPDATE `Mokejimas`
					SET    `data`='{$data['data']}',
					       `pervedama_suma`='{$data['pervedama_suma']}',
						   `fk_Registracijaid_Registracija`='{$data['fk_Registracijaid_Registracija']}',
						   `fk_Komandaid_Komanda`='{$data['fk_Komandaid_Komanda']}'
					WHERE `id_Mokejimas`='{$data['id_Mokejimas']}'";
		mysql::query($query);
	}
	/**
	 * Tvarkaraščio įrašymas
	 * @param type $data
	 */
	public function insertMokejimas($data) {
		$query = "  INSERT INTO `Mokejimas`
								(
									`data`,
									`pervedama_suma`,
									`fk_Registracijaid_Registracija`,
									`fk_Komandaid_Komanda`,
									`id_Mokejimas`
								)
								VALUES
								(
									'{$data['data']}',
									'{$data['pervedama_suma']}',
									'{$data['fk_Registracijaid_Registracija']}',
									'{$data['fk_Komandaid_Komanda']}',
									'{$data['id_Mokejimas']}'
								)";
		mysql::query($query);
	}
	
	/**
	 * Tvarkaraščio šalinimas
	 * @param type $id
	 */
	public function deleteMokejimas($id) {
		$query = "  DELETE FROM `Mokejimas`
					WHERE `id_Mokejimas`='{$id}'";
		mysql::query($query);
	}
	
	/**
	 * Nurodyto Tvarkaraščio kiekio radimas
	 * @param type $id
	 * @return type
	 */

	
	/**
	 * Didžiausios Tvarkaraščio id reikšmės radimas
	 * @return type
	 */
	public function getMaxIdOfMokejimas() {
		$query = "  SELECT MAX(`id_Mokejimas`) AS `latestId`
					FROM `Mokejimas`";
		$data = mysql::select($query);
		
		return $data[0]['latestId'];
	}
	
}