<?php

/**
 * Boulingo varžybų tvarkaraščių klasė
 *
 * @author Jonas Mičiulis IF-4/14
 */

class Tvarkarastis {
	
	public function __construct() {
		
	}
	
	/**
	 * Tvarkaraščio išrinkimas
	 * @param type $id
	 * @return type
	 */
	 public function getTvarkarastis($id) {
		$query = "  SELECT *
					FROM `Tvarkarastis`
					WHERE `id_Tvarkarastis`='{$id}'";
		$data = mysql::select($query);
		
		return $data[0];
	}

	/**
	 * Tvarkaraščio sąrašo išrinkimas
	 * @param type $limit
	 * @param type $offset
	 * @return type
	 */
	public function getTvarkarastisList($limit = null, $offset = null) {
		$limitOffsetString = "";
		if(isset($limit)) {
			$limitOffsetString .= " LIMIT {$limit}";
		}
		if(isset($offset)) {
			$limitOffsetString .= " OFFSET {$offset}";
		}
		
		$query = "  SELECT `Tvarkarastis`.`kovos_data`,
						   `Tvarkarastis`.`kovos_laikas`,
						   `Tvarkarastis`.`id_Tvarkarastis`
					FROM `Tvarkarastis`
						LEFT JOIN `Varzybos`
							ON `Tvarkarastis`.`fk_Varzybosid_Varzybos`=`Varzybos`.`id_Varzybos` LIMIT {$limit} OFFSET {$offset}";
		$data = mysql::select($query);
		
		return $data;
	}

	/**
	 * Tvarkaraščio kiekio radimas
	 * @return type
	 */
	public function getTvarkarastisListCount() {
		$query = "  SELECT COUNT(`Tvarkarastis`.`id_Tvarkarastis`) as `kiekis`
					FROM `Tvarkarastis`";
		$data = mysql::select($query);
		
		return $data[0]['kiekis'];
	}
	
	/**
	 * Tvarkaraščio išrinkimas pagal personalą
	 * @param type $brandId
	 * @return type
	 */
	 	public function getTvarkarastisListByVarzybos($brandId) {
		$query = "  SELECT *
					FROM `Tvarkarastis`
					WHERE `id_Tvarkarastis`='{$brandId}'";
		$data = mysql::select($query);
		
		return $data;
	}

	/**
	 * Tvarkaraščio atnaujinimas
	 * @param type $data
	 */
	public function updateTvarkarastis($data) {
		$query = "  UPDATE `Tvarkarastis`
					SET    `kovos_data`='{$data['kovos_data']}',
					       `kovos_laikas`='{$data['kovos_laikas']}',
						   `fk_Varzybosid_Varzybos`='{$data['fk_Varzybosid_Varzybos']}'
					WHERE `id_Tvarkarastis`='{$data['id_Tvarkarastis']}'";
		mysql::query($query);
	}
	/**
	 * Tvarkaraščio įrašymas
	 * @param type $data
	 */
	public function insertTvarkarastis($data) {
		$query = "  INSERT INTO `Tvarkarastis`
								(
									`kovos_data`,
									`kovos_laikas`,
									`fk_Varzybosid_Varzybos`,
									`id_Tvarkarastis`
								)
								VALUES
								(
									'{$data['kovos_data']}',
									'{$data['kovos_laikas']}',
									'{$data['fk_Varzybosid_Varzybos']}',
									'{$data['id_Tvarkarastis']}'
								)";
		mysql::query($query);
	}
	
	/**
	 * Tvarkaraščio šalinimas
	 * @param type $id
	 */
	public function deleteTvarkarastis($id) {
		$query = "  DELETE FROM `Tvarkarastis`
					WHERE `id_Tvarkarastis`='{$id}'";
		mysql::query($query);
	}
	
	/**
	 * Nurodyto Tvarkaraščio kiekio radimas
	 * @param type $id
	 * @return type
	 */

	
	/**
	 * Didžiausios Tvarkaraščio id reikšmės radimas
	 * @return type
	 */
	public function getMaxIdOfTvarkarastis() {
		$query = "  SELECT MAX(`id_Tvarkarastis`) AS `latestId`
					FROM `Tvarkarastis`";
		$data = mysql::select($query);
		
		return $data[0]['latestId'];
	}
	
}