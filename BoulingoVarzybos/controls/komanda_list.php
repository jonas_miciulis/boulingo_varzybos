<?php

	// sukuriame markių klasės objektą
	include 'libraries/komanda.class.php';
	$brandsObj = new komanda();
	
	// sukuriame puslapiavimo klasės objektą
	include 'utils/paging.class.php';
	$paging = new paging(NUMBER_OF_ROWS_IN_PAGE);
	
	if(!empty($removeId)) {
		// šaliname komandą
		$removeErrorParameter = '';
		$brandsObj->deleteKomanda($removeId);

		// nukreipiame į komandų puslapį
		header("Location: index.php?module={$module}");
		die();
	}
?>
<ul id="pagePath">
	<li><a href="index.php">Pradžia</a></li>
	<li>Komandos</li>
</ul>
<div id="actions">
	<a href='index.php?module=<?php echo $module; ?>&action=new'>Nauja komanda</a>
</div>
<div class="float-clear"></div>

<?php if(isset($_GET['remove_error'])) { ?>
	<div class="errorBox">
		Komanda nebuvo pašalinta.
	</div>
<?php } ?>

<table>
	<tr>
		<th>Komandos ID</th>
		<th>Pavadinimas</th>
		<th>Narių skaičius</th>
		<th>Reitingas</th>
		<th>Laimėtų turnyrų sk</th>
		<th>Telefono nr</th>
		<th></th>
	</tr>
	<?php
		// suskaičiuojame bendrą įrašų kiekį
		$elementCount = $brandsObj->getKomandaListCount();

		// suformuojame sąrašo puslapius
		$paging->process($elementCount, $pageId);

		// išrenkame nurodyto puslapio markes
		$data = $brandsObj->getKomandaList($paging->size, $paging->first);

		// suformuojame lentelę
		foreach($data as $key => $val) {
			echo
				"<tr>"
					. "<td>{$val['id_Komanda']}</td>"
					. "<td>{$val['pavadinimas']}</td>"
					. "<td>{$val['komandos_nariu_skaicius']}</td>"
					. "<td>{$val['reitingas']}</td>"
					. "<td>{$val['laimetu_turnyru_sk']}</td>"
					. "<td>{$val['komandos_telefono_nr']}</td>"
					. "<td>"
						. "<a href='#' onclick='showConfirmDialog(\"{$module}\", \"{$val['id_Komanda']}\"); return false;' title=''>šalinti</a>&nbsp;"
						. "<a href='index.php?module={$module}&id={$val['id_Komanda']}' title=''>redaguoti</a>"
					. "</td>"
				. "</tr>";
		}
	?>
</table>

<?php
	// įtraukiame puslapių šabloną
	include 'controls/paging.php';
?>