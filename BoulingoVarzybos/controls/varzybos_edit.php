<?php
	include 'libraries/strukturos.class.php';
	$brandsObj = new strukturos();
	
	include 'libraries/varzybos.class.php';
	$customersObj = new varzybos();
	
	$formErrors = null;
	$fields = array();
	
	// nustatome privalomus laukus
	$required = array('id_Varzybos', 'pavadinimas', 'varzybų_pradzia', 'varzybų_pabaiga', 'struktura');
	
	// maksimalūs leidžiami laukų ilgiai
	$maxLengths = array (
		'id_Varzybos' => 20
	);
	
	// vartotojas paspaudė išsaugojimo mygtuką
	if(!empty($_POST['submit'])) {
		include 'utils/validator.class.php';
		
		// nustatome laukų validatorių tipus
		$validations = array (
			'id_Varzybos' => 'positivenumber',
			'pavadinimas' => 'anything',
			'varzybų_pradzia' => 'date',
			'varzybų_pabaiga' => 'date',
			'struktura' => 'anything'
			);
		
		// sukuriame laukų validatoriaus objektą
		$validator = new validator($validations, $required, $maxLengths);

		// laukai įvesti be klaidų
		if($validator->validate($_POST)) {
			// suformuojame laukų reikšmių masyvą SQL užklausai
			$dataN = $validator->preparePostFieldsForSQL();
			
			
			
			if(isset($dataN['editing'])) {
				// redaguojame varzybas
				$customersObj->updateVarzybos($dataN);
			} else {
				// įrašome naujas varzybas
				$customersObj->insertVarzybos($dataN);
			}

			// nukreipiame vartotoją į klientų puslapį
			header("Location: index.php?module={$module}");
			die();
		}
		else {
			// gauname klaidų pranešimą
			$formErrors = $validator->getErrorHTML();
			// laukų reikšmių kintamajam priskiriame įvestų laukų reikšmes
			$fields = $_POST;
		}
	} else {
		// tikriname, ar nurodytas elemento id. Jeigu taip, išrenkame elemento duomenis ir jais užpildome formos laukus.
		if(!empty($id)) {
			// išrenkame varžybas
			$fields = $customersObj->getVarzybos($id);
			$fields['editing'] = 1;
		}
	}
?>
<ul id="pagePath">
	<li><a href="index.php">Pradžia</a></li>
	<li><a href="index.php?module=<?php echo $module; ?>">Varžybos</a></li>
	<li><?php if(!empty($id)) echo "Varžybų redagavimas"; else echo "Naujos varžybos"; ?></li>
</ul>
<div class="float-clear"></div>
<div id="formContainer">
	<?php if($formErrors != null) { ?>
		<div class="errorBox">
			Neįvesti arba neteisingai įvesti šie laukai:
			<?php 
				echo $formErrors;
			?>
		</div>
	<?php } ?>
	<form action="" method="post">
		<fieldset>
			<legend>Varžybų informacija</legend>
			<p>
				<label class="field" for="pavadinimas">Pavadinimas<?php echo in_array('pavadinimas', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="pavadinimas" name="pavadinimas" class="textbox-150" value="<?php echo isset($fields['pavadinimas']) ? $fields['pavadinimas'] : ''; ?>">
				<?php if(key_exists('pavadinimas', $maxLengths)) echo "<span class='max-len'>(iki {$maxLengths['pavadinimas']} simb.)</span>"; ?>
			</p>
			<p>
				<label class="field" for="varzybų_pradzia">Pradžia<?php echo in_array('varzybų_pradzia', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="varzybų_pradzia" name="varzybų_pradzia" class="date textbox-70" value="<?php echo isset($fields['varzybų_pradzia']) ? $fields['varzybų_pradzia'] : ''; ?>">
				<?php if(key_exists('varzybų_pradzia', $maxLengths)) echo "<span class='max-len'>(iki {$maxLengths['varzybų_pradzia']} simb.)</span>"; ?>
			</p>
			<p>
				<label class="field" for="varzybų_pabaiga">Pabaiga<?php echo in_array('varzybų_pabaiga', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="varzybų_pabaiga" name="varzybų_pabaiga" class="date textbox-70" value="<?php echo isset($fields['varzybų_pabaiga']) ? $fields['varzybų_pabaiga'] : ''; ?>">
				<?php if(key_exists('varzybų_pabaiga', $maxLengths)) echo "<span class='max-len'>(iki {$maxLengths['varzybų_pabaiga']} simb.)</span>"; ?>
			</p>
			<p>
				<label class="field" for="organizatoriu_sk">Organizatorių skaičius<?php echo in_array('organizatoriu_sk', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="organizatoriu_sk" name="organizatoriu_sk" class="textbox-150" value="<?php echo isset($fields['organizatoriu_sk']) ? $fields['organizatoriu_sk'] : ''; ?>">
				<?php if(key_exists('organizatoriu_sk', $maxLengths)) echo "<span class='max-len'>(iki {$maxLengths['organizatoriu_sk']} simb.)</span>"; ?>
			</p>
			<p>
				<label class="field" for="prizinis_fondas">Prizinis fondas<?php echo in_array('prizinis_fondas', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="prizinis_fondas" name="prizinis_fondas" class="textbox-150" value="<?php echo isset($fields['prizinis_fondas']) ? $fields['prizinis_fondas'] : ''; ?>">
				<?php if(key_exists('prizinis_fondas', $maxLengths)) echo "<span class='max-len'>(iki {$maxLengths['prizinis_fondas']} simb.)</span>"; ?>
			</p>
			<p>
				<label class="field" for="pagrindiniai_remejai">Pagrindiniai rėmėjai<?php echo in_array('pagrindiniai_remejai', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="pagrindiniai_remejai" name="pagrindiniai_remejai" class="textbox-150" value="<?php echo isset($fields['pagrindiniai_remejai']) ? $fields['pagrindiniai_remejai'] : ''; ?>">
				<?php if(key_exists('pagrindiniai_remejai', $maxLengths)) echo "<span class='max-len'>(iki {$maxLengths['pagrindiniai_remejai']} simb.)</span>"; ?>
			</p>
			<p>
				<label class="field" for="prizininku_sk">Prizininkų skaičius<?php echo in_array('prizininku_sk', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="prizininku_sk" name="prizininku_sk" class="textbox-150" value="<?php echo isset($fields['prizininku_sk']) ? $fields['prizininku_sk'] : ''; ?>">
				<?php if(key_exists('prizininku_sk', $maxLengths)) echo "<span class='max-len'>(iki {$maxLengths['prizininku_sk']} simb.)</span>"; ?>
			</p>
			
			<p>
				<label class="field" for="struktura">Struktūra<?php echo in_array('struktura', $required) ? '<span> *</span>' : ''; ?></label>
				<select id="struktura" name="struktura">
					<option value="-1">--------</option>
					<?php
						// išrenkame visas markes
						$brands = $brandsObj->getStukturosList();
						foreach($brands as $key => $val) {
							$selected = "";
							if(isset($fields['struktura']) && $fields['struktura'] == $val['id_strukturos']) {
								$selected = " selected='selected'";
							}
							echo "<option{$selected} value='{$val['id_strukturos']}'>{$val['name']}</option>";
						}
					?>
				</select>
			</p>
			
			<p>
				<?php if(!isset($fields['editing'])) { ?>
					<label class="field" for="id_Varzybos">Varžybų ID<?php echo in_array('id_Varzybos', $required) ? '<span> *</span>' : ''; ?></label>
					<input type="text" id="id_Varzybos" name="id_Varzybos" class="textbox-70" value="<?php echo isset($fields['id_Varzybos']) ? $fields['id_Varzybos'] : ''; ?>">
					<?php if(key_exists('id_Varzybos', $maxLengths)) echo "<span class='max-len'>(iki {$maxLengths['id_Varzybos']} simb.)</span>"; ?>
				<?php } else { ?>
						<label class="field" for="id_Varzybos">Varžybų ID</label>
						<span class="input-value"><?php echo $fields['id_Varzybos']; ?></span>
						<input type="hidden" name="editing" value="1" />
						<input type="hidden" name="id_Varzybos" value="<?php echo $fields['id_Varzybos']; ?>" />
				<?php } ?>
			</p>
			
		</fieldset>
			
		<fieldset>
			<legend>Registracijos informacija</legend>
			<p>
				<label class="field" for="reg_pradzios_data">Registracijos pradžia<?php echo in_array('reg_pradzios_data', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="reg_pradzios_data" name="reg_pradzios_data" class="date textbox-70" value="<?php echo isset($fields['reg_pradzios_data']) ? $fields['reg_pradzios_data'] : ''; ?>">
			</p>
			<p>
				<label class="field" for="reg_pabaigos_data">Registracijos pabaiga<?php echo in_array('reg_pabaigos_data', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="reg_pabaigos_data" name="reg_pabaigos_data" class="date textbox-70" value="<?php echo isset($fields['reg_pabaigos_data']) ? $fields['reg_pabaigos_data'] : ''; ?>">
			</p>
			<p>
				<label class="field" for="min_komandu_sk">Min komandų kiekis<?php echo in_array('min_komandu_sk', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="min_komandu_sk" name="min_komandu_sk" class="textbox-70" value="<?php echo isset($fields['min_komandu_sk']) ? $fields['min_komandu_sk'] : ''; ?>">
			</p>
			<p>
				<label class="field" for="max_komandu_sk">Max komandų kiekis<?php echo in_array('max_komandu_sk', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="max_komandu_sk" name="max_komandu_sk" class="textbox-70" value="<?php echo isset($fields['max_komandu_sk']) ? $fields['max_komandu_sk'] : ''; ?>">
			</p>
			<p>
				<label class="field" for="startinis_mokestis">Startinis mokestis<?php echo in_array('startinis_mokestis', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="startinis_mokestis" name="startinis_mokestis" class="textbox-150" value="<?php echo isset($fields['startinis_mokestis']) ? $fields['startinis_mokestis'] : ''; ?>">
			</p>
		</fieldset>

		<p class="required-note">* pažymėtus laukus užpildyti privaloma</p>
		<p>
			<input type="submit" class="submit" name="submit" value="Išsaugoti">
		</p>
	</form>
</div>