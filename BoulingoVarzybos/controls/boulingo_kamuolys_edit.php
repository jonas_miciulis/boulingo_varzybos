<?php
	include 'libraries/svoriai.class.php';
	$brands2Obj = new svoriai();
	
	include 'libraries/boulingo_klubas.class.php';
	$brandsObj = new boulingo_klubas();

	include 'libraries/boulingo_kamuolys.class.php';
	$customersObj = new boulingo_kamuolys();
	
	$formErrors = null;
	$fields = array();
	
	// nustatome privalomus laukus
	$required = array('id_Boulingo_kamuolys', 'fk_Boulingo_klubasid_Boulingo_klubas', 'kaina', 'svoris_svarais');
	
	// maksimalūs leidžiami laukų ilgiai
	$maxLengths = array (
		'id_Boulingo_kamuolys' => 20
	);
	
	// vartotojas paspaudė išsaugojimo mygtuką
	if(!empty($_POST['submit'])) {
		include 'utils/validator.class.php';
		
		// nustatome laukų validatorių tipus
		$validations = array (
			'id_Boulingo_kamuolys' => 'positivenumber',
			'kaina' => 'positivenumber',
			'svoris_svarais' => 'anything',
			'fk_Boulingo_klubasid_Boulingo_klubas' => 'positivenumber');
		
		// sukuriame laukų validatoriaus objektą
		$validator = new validator($validations, $required, $maxLengths);

		// laukai įvesti be klaidų
		if($validator->validate($_POST)) {
			// suformuojame laukų reikšmių masyvą SQL užklausai
			$dataN = $validator->preparePostFieldsForSQL();
			
			
			
			if(isset($dataN['editing'])) {
				// redaguojame klientą
				$customersObj->updateBoulingoKamuolys($dataN);
			} else {
				// įrašome naują klientą
				$customersObj->insertBoulingoKamuolys($dataN);
			}

			// nukreipiame vartotoją į klientų puslapį
			header("Location: index.php?module={$module}");
			die();
		}
		else {
			// gauname klaidų pranešimą
			$formErrors = $validator->getErrorHTML();
			// laukų reikšmių kintamajam priskiriame įvestų laukų reikšmes
			$fields = $_POST;
		}
	} else {
		// tikriname, ar nurodytas elemento id. Jeigu taip, išrenkame elemento duomenis ir jais užpildome formos laukus.
		if(!empty($id)) {
			// išrenkame tvarkaraštį
			$fields = $customersObj->getBoulingoKamuolys($id);
			$fields['editing'] = 1;
		}
	}
?>
<ul id="pagePath">
	<li><a href="index.php">Pradžia</a></li>
	<li><a href="index.php?module=<?php echo $module; ?>">Boulingo kamuolys</a></li>
	<li><?php if(!empty($id)) echo "Kliento redagavimas"; else echo "Naujas boulingo kamuolys"; ?></li>
</ul>
<div class="float-clear"></div>
<div id="formContainer">
	<?php if($formErrors != null) { ?>
		<div class="errorBox">
			Neįvesti arba neteisingai įvesti šie laukai:
			<?php 
				echo $formErrors;
			?>
		</div>
	<?php } ?>
	<form action="" method="post">
		<fieldset>
			<legend>Boulingo kamuolio informacija</legend>
			
			<p>
				<?php if(!isset($fields['editing'])) { ?>
					<label class="field" for="id_Boulingo_kamuolys">Boulingo kamuolio ID<?php echo in_array('id_Boulingo_kamuolys', $required) ? '<span> *</span>' : ''; ?></label>
					<input type="text" id="id_Boulingo_kamuolys" name="id_Boulingo_kamuolys" class="textbox-70" value="<?php echo isset($fields['id_Boulingo_kamuolys']) ? $fields['id_Boulingo_kamuolys'] : ''; ?>">
					<?php if(key_exists('id_Boulingo_kamuolys', $maxLengths)) echo "<span class='max-len'>(iki {$maxLengths['id_Boulingo_kamuolys']} simb.)</span>"; ?>
				<?php } else { ?>
						<label class="field" for="id_Boulingo_kamuolys">Boulingo kamuolio ID</label>
						<span class="input-value"><?php echo $fields['id_Boulingo_kamuolys']; ?></span>
						<input type="hidden" name="editing" value="1" />
						<input type="hidden" name="id_Boulingo_kamuolys" value="<?php echo $fields['id_Boulingo_kamuolys']; ?>" />
				<?php } ?>
			</p>
			
			<p>
				<label class="field" for="kaina">Kaina<?php echo in_array('kaina', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="kaina" name="kaina" class="textbox-150" value="<?php echo isset($fields['kaina']) ? $fields['kaina'] : ''; ?>">
			</p>
			<p>
				<label class="field" for="svoris_svarais">Svoris svarais<?php echo in_array('svoris_svarais', $required) ? '<span> *</span>' : ''; ?></label>
				<select id="svoris_svarais" name="svoris_svarais">
					<option value="-1">--------</option>
					<?php
						// išrenkame visas markes
						$brands2 = $brands2Obj->getSvoriaiList();
						foreach($brands2 as $key => $val) {
							$selected = "";
							if(isset($fields['svoris_svarais']) && $fields['svoris_svarais'] == $val['id_svoriai']) {
								$selected = " selected='selected'";
							}
							echo "<option{$selected} value='{$val['id_svoriai']}'>{$val['name']}</option>";
						}
					?>
				</select>
			</p>
			
			<p>
				<label class="field" for="brand">Boulingo klubas<?php echo in_array('fk_Boulingo_klubasid_Boulingo_klubas', $required) ? '<span> *</span>' : ''; ?></label>
				<select id="brand" name="fk_Boulingo_klubasid_Boulingo_klubas">
					<option value="-1">--------</option>
					<?php
						// išrenkame visas ---
						$brands = $brandsObj->getBoulingoKlubasList();
						foreach($brands as $key => $val) {
							$selected = "";
							if(isset($fields['fk_Boulingo_klubasid_Boulingo_klubas']) && $fields['fk_Boulingo_klubasid_Boulingo_klubas'] == $val['id_Boulingo_klubas']) {
								$selected = " selected='selected'";
							}
							echo "<option{$selected} value='{$val['id_Boulingo_klubas']}'>{$val['pavadinimas']}</option>";
						}
					?>
				</select>
			</p>

			
		</fieldset>
		<p class="required-note">* pažymėtus laukus užpildyti privaloma</p>
		<p>
			<input type="submit" class="submit" name="submit" value="Išsaugoti">
		</p>
	</form>
</div>