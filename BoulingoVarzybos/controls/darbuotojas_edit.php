﻿<?php
	
	include 'libraries/personalas.class.php';
	$brandsObj = new personalas();

	include 'libraries/darbuotojas.class.php';
	$customersObj = new darbuotojas();
	
	$formErrors = null;
	$fields = array();
	
	// nustatome privalomus laukus
	$required = array('vardas', 'pavarde', 'fk_Personalasid_Personalas', 'id_Darbuotojas');
	
	// maksimalūs leidžiami laukų ilgiai
	$maxLengths = array (
	//	'id_Darbuotojas' => 20
	);
	
	// vartotojas paspaudė išsaugojimo mygtuką
	if(!empty($_POST['submit'])) {
		include 'utils/validator.class.php';
		
		// nustatome laukų validatorių tipus
		$validations = array (
			'vardas' => 'anything',
			'pavarde' => 'anything',
			'id_Darbuotojas' => 'positivenumber',
			'fk_Personalasid_Personalas' => 'positivenumber');
		
		// sukuriame laukų validatoriaus objektą
		$validator = new validator($validations, $required, $maxLengths);

		// laukai įvesti be klaidų
		if($validator->validate($_POST)) {
			// suformuojame laukų reikšmių masyvą SQL užklausai
			$data = $validator->preparePostFieldsForSQL();
			
			
			
			if(isset($data['editing'])) {
				// redaguojame klientą
				$customersObj->updateDarbuotojas($data);
			} else {
				// įrašome naują klientą
				$customersObj->insertDarbuotojas($data);
			}

			// nukreipiame vartotoją į klientų puslapį
			header("Location: index.php?module={$module}");
			die();
		}
		else {
			// gauname klaidų pranešimą
			$formErrors = $validator->getErrorHTML();
			// laukų reikšmių kintamajam priskiriame įvestų laukų reikšmes
			$fields = $_POST;
		}
	} else {
		// tikriname, ar nurodytas elemento id. Jeigu taip, išrenkame elemento duomenis ir jais užpildome formos laukus.
		if(!empty($id)) {
			// išrenkame klientą
			$fields = $customersObj->getDarbuotojas($id);
			$fields['editing'] = 1;
		}
	}
?>
<ul id="pagePath">
	<li><a href="index.php">Pradžia</a></li>
	<li><a href="index.php?module=<?php echo $module; ?>">Darbuotojai</a></li>
	<li><?php if(!empty($id)) echo "Darbuotojo redagavimas"; else echo "Naujas darbuotojas"; ?></li>
</ul>
<div class="float-clear"></div>
<div id="formContainer">
	<?php if($formErrors != null) { ?>
		<div class="errorBox">
			Neįvesti arba neteisingai įvesti šie laukai:
			<?php 
				echo $formErrors;
			?>
		</div>
	<?php } ?>
	<form action="" method="post">
		<fieldset>
			<legend>Darbuotojo informacija</legend>
			<p>
				<label class="field" for="vardas">Vardas<?php echo in_array('vardas', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="vardas" name="vardas" class="textbox-150" value="<?php echo isset($fields['vardas']) ? $fields['vardas'] : ''; ?>">
			</p>
			<p>
				<label class="field" for="pavarde">Pavardė<?php echo in_array('pavarde', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="pavarde" name="pavarde" class="textbox-150" value="<?php echo isset($fields['pavarde']) ? $fields['pavarde'] : ''; ?>">
			</p>
			
			<p>
				<?php if(!isset($fields['editing'])) { ?>
					<label class="field" for="id_Darbuotojas">Darbuotojo ID<?php echo in_array('id_Darbuotojas', $required) ? '<span> *</span>' : ''; ?></label>
					<input type="text" id="id_Darbuotojas" name="id_Darbuotojas" class="textbox-70" value="<?php echo isset($fields['id_Darbuotojas']) ? $fields['id_Darbuotojas'] : ''; ?>">
					<?php if(key_exists('id_Darbuotojas', $maxLengths)) echo "<span class='max-len'>(iki {$maxLengths['id_Darbuotojas']} simb.)</span>"; ?>
				<?php } else { ?>
						<label class="field" for="id_Darbuotojas">Darbuotojo ID</label>
						<span class="input-value"><?php echo $fields['id_Darbuotojas']; ?></span>
						<input type="hidden" name="editing" value="1" />
						<input type="hidden" name="id_Darbuotojas" value="<?php echo $fields['id_Darbuotojas']; ?>" />
				<?php } ?>
			</p>
			
			<p>
				<label class="field" for="brand">Personalo ID<?php echo in_array('fk_Personalasid_Personalas', $required) ? '<span> *</span>' : ''; ?></label>
				<select id="brand" name="fk_Personalasid_Personalas">
					<option value="-1">--------</option>
					<?php
						// išrenkame visus ---
						$brands = $brandsObj->getPersonalasList();
						foreach($brands as $key => $val) {
							$selected = "";
							if(isset($fields['fk_Personalasid_Personalas']) && $fields['fk_Personalasid_Personalas'] == $val['id_Personalas']) {
								$selected = " selected='selected'";
							}
							echo "<option{$selected} value='{$val['id_Personalas']}'>{$val['id_Personalas']}</option>";
						}
					?>
				</select>
			</p>
			
		</fieldset>
		<p class="required-note">* pažymėtus laukus užpildyti privaloma</p>
		<p>
			<input type="submit" class="submit" name="submit" value="Išsaugoti">
		</p>
	</form>
</div>