<?php
	
	include 'libraries/komanda.class.php';
	$brandsObj = new komanda();

	include 'libraries/dalyvis.class.php';
	$customersObj = new dalyvis();
	
	$formErrors = null;
	$fields = array();
	
	// nustatome privalomus laukus
	$required = array('id_Dalyvis', 'fk_Komandaid_Komanda', 'vardas', 'pavarde');
	
	// maksimalūs leidžiami laukų ilgiai
	$maxLengths = array (
		'id_Dalyvis' => 20
	);
	
	// vartotojas paspaudė išsaugojimo mygtuką
	if(!empty($_POST['submit'])) {
		include 'utils/validator.class.php';
		
		// nustatome laukų validatorių tipus
		$validations = array (
			'id_Dalyvis' => 'positivenumber',
			'vardas' => 'anything',
			'pavarde' => 'anything',
			'fk_Komandaid_Komanda' => 'positivenumber');
		
		// sukuriame laukų validatoriaus objektą
		$validator = new validator($validations, $required, $maxLengths);

		// laukai įvesti be klaidų
		if($validator->validate($_POST)) {
			// suformuojame laukų reikšmių masyvą SQL užklausai
			$dataN = $validator->preparePostFieldsForSQL();
			
			
			
			if(isset($dataN['editing'])) {
				// redaguojame dalyvį
				$customersObj->updateDalyvis($dataN);
			} else {
				// įrašome naują dalyvį
				$customersObj->insertDalyvis($dataN);
			}

			// nukreipiame vartotoją į klientų puslapį
			header("Location: index.php?module={$module}");
			die();
		}
		else {
			// gauname klaidų pranešimą
			$formErrors = $validator->getErrorHTML();
			// laukų reikšmių kintamajam priskiriame įvestų laukų reikšmes
			$fields = $_POST;
		}
	} else {
		// tikriname, ar nurodytas elemento id. Jeigu taip, išrenkame elemento duomenis ir jais užpildome formos laukus.
		if(!empty($id)) {
			// išrenkame dalyvį
			$fields = $customersObj->getDalyvis($id);
			$fields['editing'] = 1;
		}
	}
?>
<ul id="pagePath">
	<li><a href="index.php">Pradžia</a></li>
	<li><a href="index.php?module=<?php echo $module; ?>">Dalyvis</a></li>
	<li><?php if(!empty($id)) echo "Kliento redagavimas"; else echo "Naujas dalyvis"; ?></li>
</ul>
<div class="float-clear"></div>
<div id="formContainer">
	<?php if($formErrors != null) { ?>
		<div class="errorBox">
			Neįvesti arba neteisingai įvesti šie laukai:
			<?php 
				echo $formErrors;
			?>
		</div>
	<?php } ?>
	<form action="" method="post">
		<fieldset>
			<legend>Dalyvio informacija</legend>
			<p>
				<label class="field" for="vardas">Vardas<?php echo in_array('vardas', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="vardas" name="vardas" class="textbox-150" value="<?php echo isset($fields['vardas']) ? $fields['vardas'] : ''; ?>">
			</p>
			<p>
				<label class="field" for="pavarde">Pavardė<?php echo in_array('pavarde', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="pavarde" name="pavarde" class="textbox-150" value="<?php echo isset($fields['pavarde']) ? $fields['pavarde'] : ''; ?>">
			</p>
			<p>
				<label class="field" for="individ_reitingas">Individualus reitingas<?php echo in_array('individ_reitingas', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="individ_reitingas" name="individ_reitingas" class="textbox-150" value="<?php echo isset($fields['individ_reitingas']) ? $fields['individ_reitingas'] : ''; ?>">
			</p>
			<p>
				<label class="field" for="vid_tasku_skaicius">Vidutinis taškų skaičius<?php echo in_array('vid_tasku_skaicius', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="vid_tasku_skaicius" name="vid_tasku_skaicius" class="textbox-150" value="<?php echo isset($fields['vid_tasku_skaicius']) ? $fields['vid_tasku_skaicius'] : ''; ?>">
			</p>
			<p>
				<label class="field" for="rekordas">Rekordas<?php echo in_array('rekordas', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="rekordas" name="rekordas" class="textbox-150" value="<?php echo isset($fields['rekordas']) ? $fields['rekordas'] : ''; ?>">
			</p>
			<p>
				<label class="field" for="laimetu_turnyru_sk">Laimėtų turnyrų skaičius<?php echo in_array('laimetu_turnyru_sk', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="laimetu_turnyru_sk" name="laimetu_turnyru_sk" class="textbox-150" value="<?php echo isset($fields['laimetu_turnyru_sk']) ? $fields['laimetu_turnyru_sk'] : ''; ?>">
			</p>
			
			<p>
				<?php if(!isset($fields['editing'])) { ?>
					<label class="field" for="id_Dalyvis">Dalyvio ID<?php echo in_array('id_Dalyvis', $required) ? '<span> *</span>' : ''; ?></label>
					<input type="text" id="id_Dalyvis" name="id_Dalyvis" class="textbox-70" value="<?php echo isset($fields['id_Dalyvis']) ? $fields['id_Dalyvis'] : ''; ?>">
					<?php if(key_exists('id_Dalyvis', $maxLengths)) echo "<span class='max-len'>(iki {$maxLengths['id_Dalyvis']} simb.)</span>"; ?>
				<?php } else { ?>
						<label class="field" for="id_Dalyvis">Dalyvio ID</label>
						<span class="input-value"><?php echo $fields['id_Dalyvis']; ?></span>
						<input type="hidden" name="editing" value="1" />
						<input type="hidden" name="id_Dalyvis" value="<?php echo $fields['id_Dalyvis']; ?>" />
				<?php } ?>
			</p>
			
			<p>
				<label class="field" for="brand">Komanda<?php echo in_array('fk_Komandaid_Komanda', $required) ? '<span> *</span>' : ''; ?></label>
				<select id="brand" name="fk_Komandaid_Komanda">
					<option value="-1">--------</option>
					<?php
						// išrenkame visas ---
						$brands = $brandsObj->getKomandaList();
						foreach($brands as $key => $val) {
							$selected = "";
							if(isset($fields['fk_Komandaid_Komanda']) && $fields['fk_Komandaid_Komanda'] == $val['id_Komanda']) {
								$selected = " selected='selected'";
							}
							echo "<option{$selected} value='{$val['id_Komanda']}'>{$val['pavadinimas']}</option>";
						}
					?>
				</select>
			</p>

			
		</fieldset>
		<p class="required-note">* pažymėtus laukus užpildyti privaloma</p>
		<p>
			<input type="submit" class="submit" name="submit" value="Išsaugoti">
		</p>
	</form>
</div>