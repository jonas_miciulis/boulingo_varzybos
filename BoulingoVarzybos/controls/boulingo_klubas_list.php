<?php

	// sukuriame boulingo klubo klasės objektą
	include 'libraries/boulingo_klubas.class.php';
	$brandsObj = new boulingo_klubas();
	
	// sukuriame puslapiavimo klasės objektą
	include 'utils/paging.class.php';
	$paging = new paging(NUMBER_OF_ROWS_IN_PAGE);
	
	if(!empty($removeId)) {
		// patikriname, ar šalinama markė nepriskirta modeliui
//		$count = $brandsObj->getBoulingoKamuolysCountOfBoulingoKlubas($removeId);
		
		$removeErrorParameter = '';
		if($count == 0) {
			// šaliname markę
			$brandsObj->deleteBoulingoKlubas($removeId);
		} else {
			// nepašalinome, nes markė priskirta modeliui, rodome klaidos pranešimą
			$removeErrorParameter = '$remove_error=1';
		}

		// nukreipiame į markių puslapį
		header("Location: index.php?module={$module}{$removeErrorParameter}");
		die();
	}
?>
<ul id="pagePath">
	<li><a href="index.php">Pradžia</a></li>
	<li>Boulingo klubai</li>
</ul>
<div id="actions">
	<a href='index.php?module=<?php echo $module; ?>&action=new'>Naujas klubas</a>
</div>
<div class="float-clear"></div>

<?php if(isset($_GET['remove_error'])) { ?>
	<div class="errorBox">
		Boulingo klubas nebuvo pašalintas.
	</div>
<?php } ?>

<table>
	<tr>
		<th>Boulingo klubo ID</th>
		<th>Pavadinimas</th>
		<th>Takų skaičius</th>
		<th>Takelio kaino valandai</th>
		<th></th>
	</tr>
	<?php
		// suskaičiuojame bendrą įrašų kiekį
		$elementCount = $brandsObj->getBoulingoKlubasListCount();

		// suformuojame sąrašo puslapius
		$paging->process($elementCount, $pageId);

		// išrenkame nurodyto puslapio markes
		$data = $brandsObj->getBoulingoKlubasList($paging->size, $paging->first);

		// suformuojame lentelę
		foreach($data as $key => $val) {
			echo
				"<tr>"
					. "<td>{$val['id_Boulingo_klubas']}</td>"
					. "<td>{$val['pavadinimas']}</td>"
					. "<td>{$val['taku_sk']}</td>"
					. "<td>{$val['takelio_kaina_valandai']}</td>"
					. "<td>"
						. "<a href='#' onclick='showConfirmDialog(\"{$module}\", \"{$val['id_Boulingo_klubas']}\"); return false;' title=''>šalinti</a>&nbsp;"
						. "<a href='index.php?module={$module}&id={$val['id_Boulingo_klubas']}' title=''>redaguoti</a>"
					. "</td>"
				. "</tr>";
		}
	?>
</table>

<?php
	// įtraukiame puslapių šabloną
	include 'controls/paging.php';
?>