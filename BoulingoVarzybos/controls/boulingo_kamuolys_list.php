<?php
	// sukuriame modelių klasės objektą
	include 'libraries/boulingo_kamuolys.class.php';
	$modelsObj = new boulingo_kamuolys();
	
	// sukuriame puslapiavimo klasės objektą
	include 'utils/paging.class.php';
	$paging = new paging(NUMBER_OF_ROWS_IN_PAGE);
	
	if(!empty($removeId)) {
		// patikriname, ar šalinamas modelis nenaudojamas, t.y. nepriskirtas jokiam automobiliui
	//	$count = $modelsObj->getCarCountOfModel($removeId);
		
		$removeErrorParameter = '';
		if($count == 0) {
			// pašaliname modelį
			$modelsObj->deleteBoulingoKamuolys($removeId);
		} else {
			// nepašalinome, nes modelis priskirtas bent vienam automobiliui, rodome klaidos pranešimą
			$removeErrorParameter = '&remove_error=1';
		}
		
		// nukreipiame į modelių puslapį
		header("Location: index.php?module={$module}{$removeErrorParameter}");
		die();
	}
?>
<ul id="pagePath">
	<li><a href="index.php">Pradžia</a></li>
	<li>Boulingo klubų parduodami kamuoliai</li>
</ul>
<div id="actions">
	<a href='index.php?module=<?php echo $module; ?>&action=new'>Naujas boulingo kamuolys</a>
</div>
<div class="float-clear"></div>

<?php if(isset($_GET['remove_error'])) { ?>
	<div class="errorBox">
		Boulingo kamuolys nebuvo pašalintas.
	</div>
<?php } ?>

<table>
	<tr>
		<th>Boulingo kamuolio ID</th>
		<th>Kaina</th>
		<th>Svoris svarais</th>
		<th></th>
	</tr>
	<?php
		// suskaičiuojame bendrą įrašų kiekį
		$elementCount = $modelsObj->getBoulingoKamuolysListCount();

		// suformuojame sąrašo puslapius
		$paging->process($elementCount, $pageId);

		// išrenkame nurodyto puslapio modelius
		$data = $modelsObj->getBoulingoKamuolysList($paging->size, $paging->first);

		// suformuojame lentelę
		foreach($data as $key => $val) {
			echo
				"<tr>"
					. "<td>{$val['id_Boulingo_kamuolys']}</td>"
					. "<td>{$val['kaina']}</td>"
					. "<td>{$val['svoris_svarais']}</td>"
					. "<td>"
						. "<a href='#' onclick='showConfirmDialog(\"{$module}\", \"{$val['id_Boulingo_kamuolys']}\"); return false;' title=''>šalinti</a>&nbsp;"
						. "<a href='index.php?module={$module}&id={$val['id_Boulingo_kamuolys']}' title=''>redaguoti</a>"
					. "</td>"
				. "</tr>";
		}
	?>
</table>

<?php
	// įtraukiame puslapių šabloną
	include 'controls/paging.php';
?>