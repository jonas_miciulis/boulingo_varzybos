<?php
include 'libraries/komanda.class.php';
	$servicesObj = new komanda();
	
	$formErrors = null;
	$fields = array();
	$formSubmitted = false;
		
	$data = array();
	if(!empty($_POST['submit'])) {
		$formSubmitted = true;

		// nustatome laukų validatorių tipus
		$validations = array (
			'dataNuo' => 'positivenumber',
			'dataIki' => 'positivenumber');
		
		// sukuriame validatoriaus objektą
		include 'utils/validator.class.php';
		$validator = new validator($validations);
		

		if($validator->validate($_POST)) {
			// suformuojame laukų reikšmių masyvą SQL užklausai
			$data = $validator->preparePostFieldsForSQL();
		} else {
			// gauname klaidų pranešimą
			$formErrors = $validator->getErrorHTML();
			// gauname įvestus laukus
			$fields = $_POST;
		}
	}
	
if($formSubmitted == true && ($formErrors == null)) { ?>
	<div id="header">
		<ul id="reportInfo">
			<li class="title">I ataskaita</li>
			<li>Sudarymo data: <span><?php echo date("Y-m-d"); ?></span></li>
			<li>Komandų reitingo reziai:
				<span>
					<?php
						if(!empty($data['dataNuo'])) {
							if(!empty($data['dataIki'])) {
								echo "nuo {$data['dataNuo']} iki {$data['dataIki']}";
							} else {
								echo "nuo {$data['dataNuo']}";
							}
						} else {
							if(!empty($data['dataIki'])) {
								echo "iki {$data['dataIki']}";
							} else {
								echo "nenurodyta";
							}
						}
					?>
				</span>
				<a href="report.php?id=1" title="Nauja ataskaita" class="newReport">nauja ataskaita</a>
			</li>
		</ul>
	</div>
<?php } ?>
<div id="content">
	<div id="contentMain">
		<?php if($formSubmitted == false || $formErrors != null) { ?>
			<div id="formContainer">
				<?php if($formErrors != null) { ?>
					<div class="errorBox">
						Neįvesti arba neteisingai įvesti šie laukai:
						<?php 
							echo $formErrors;
						?>
					</div>
				<?php } ?>
				<form action="" method="post">
					<fieldset>
						<legend>Įveskite ataskaitos kriterijus</legend>
						<p><label class="field" for="dataNuo">Komandos reitingas nuo</label><input type="text" id="dataNuo" name="dataNuo" class="textbox-100" value="<?php echo isset($fields['dataNuo']) ? $fields['dataNuo'] : ''; ?>" /></p>
						<p><label class="field" for="dataIki">Komandos reitingas iki</label><input type="text" id="dataIki" name="dataIki" class="textbox-100" value="<?php echo isset($fields['dataIki']) ? $fields['dataIki'] : ''; ?>" /></p>
					</fieldset>
					<p><input type="submit" class="submit" name="submit" value="Sudaryti ataskaitą"></p>
				</form>
			</div>
		<?php } else {
			

					// išrenkame ataskaitos duomenis
                                $salisData = $servicesObj->getKomandas($data['dataNuo'], $data['dataIki']);
								$statsData = $servicesObj->getKomandosStats($data['dataNuo'], $data['dataIki']);
								$stats2Data = $servicesObj->getNelaimejusios($data['dataNuo'], $data['dataIki']);
				
								if(sizeof($salisData) > 0) { ?>
		
                                    <table class="reportTable">
                                        <tr>
                                            <th>Pavadinimas</th>
                                            <th>Komandos narių sk</th>
                                            <th>Komandos reitingas</th>
                                            <th>Laimėtų turnyrų sk</th>
                                        </tr>

                                        <?php

                                            // suformuojame lentelę
                                            foreach($salisData as $key=>$val){
                                                echo "<tr>"
                                                        . "<td>{$val['pavadinimas']}</td>"
                                                        . "<td>{$val['komandos_nariu_skaicius']}</td>"
                                                        . "<td>{$val['reitingas']}</td>"
                                                        . "<td>{$val['laimetu_turnyru_sk']}</td>"
                                                    . "</tr>";
                                            }
                                        ?>
										<tr class="aggregate">
											<td class="label">Žaidėjų kiekis:</td>
											<td class="border"><?php echo "{$statsData[0]['kiekis']}"; ?></td>
											<td class="label">Komandų be titulo kiekis:</td>
											<td class="border"><?php echo "{$stats2Data[0]['kiekis2']}"; ?></td>
										</tr>
                                    </table>
			<?php   } else { ?>
                                            <div class="warningBox">
                                                Nėra tokio reitingo komandų!
                                            </div>
					<?php
					}
			} ?>
    </div>
</div>