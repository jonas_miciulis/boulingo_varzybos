<?php
	// sukuriame modelių klasės objektą
	include 'libraries/varzybos.class.php';
	$modelsObj = new varzybos();
	
	include 'libraries/strukturos.class.php';
	$models2Obj = new strukturos();
	
	// sukuriame puslapiavimo klasės objektą
	include 'utils/paging.class.php';
	$paging = new paging(NUMBER_OF_ROWS_IN_PAGE);
	
	if(!empty($removeId)) {
		// patikriname, ar šalinamas modelis nenaudojamas, t.y. nepriskirtas jokiam automobiliui
	//	$count = $modelsObj->getCarCountOfModel($removeId);
		
		$removeErrorParameter = '';
		if($count == 0) {
			// pašaliname modelį
			$modelsObj->deleteVarzybos($removeId);
		} else {
			// nepašalinome, nes modelis priskirtas bent vienam automobiliui, rodome klaidos pranešimą
			$removeErrorParameter = '&remove_error=1';
		}
		
		// nukreipiame į modelių puslapį
		header("Location: index.php?module={$module}{$removeErrorParameter}");
		die();
	}
?>
<ul id="pagePath">
	<li><a href="index.php">Pradžia</a></li>
	<li>Boulingo varžybos</li>
</ul>
<div id="actions">
	<a href='index.php?module=<?php echo $module; ?>&action=new'>Naujos varžybos</a>
</div>
<div class="float-clear"></div>

<?php if(isset($_GET['remove_error'])) { ?>
	<div class="errorBox">
		Varžybos nebuvo pašalintos.
	</div>
<?php } ?>

<table>
	<tr>
	    <th>Pavadinimas</th>
		<th>Varžybų pradžia</th>
		<th>Varžybų pabaiga</th>
		<th>Prizinis fondas</th>
		<th>Pagrindiniai rėmėjai</th>
		<th>Prizininkų skaičius</th>
		<th>Struktūra</th>
		<th></th>
	</tr>
	<?php
		// suskaičiuojame bendrą įrašų kiekį
		$elementCount = $modelsObj->getVarzybosListCount();

		// suformuojame sąrašo puslapius
		$paging->process($elementCount, $pageId);

		// išrenkame nurodyto puslapio modelius
		$data = $modelsObj->getVarzybosList($paging->size, $paging->first);

		// suformuojame lentelę
		foreach($data as $key => $val) {
			echo
				"<tr>"
//					. "<td>{$val['id_Varzybos']}</td>"
				    . "<td>{$val['pavadinimas']}</td>"
					. "<td>{$val['varzybų_pradzia']}</td>"
					. "<td>{$val['varzybų_pabaiga']}</td>"
//					. "<td>{$val['organizatoriu_sk']}</td>"
					. "<td>{$val['prizinis_fondas']}</td>"
					. "<td>{$val['pagrindiniai_remejai']}</td>"
					. "<td>{$val['prizininku_sk']}</td>"
					. "<td>{$val['struktura']}</td>"
					. "<td>"
						. "<a href='#' onclick='showConfirmDialog(\"{$module}\", \"{$val['id_Varzybos']}\"); return false;' title=''>šalinti</a>&nbsp;"
						. "<a href='index.php?module={$module}&id={$val['id_Varzybos']}' title=''>redaguoti</a>"
					. "</td>"
				. "</tr>";
		}
	?>
</table>

<?php
	// įtraukiame puslapių šabloną
	include 'controls/paging.php';
?>