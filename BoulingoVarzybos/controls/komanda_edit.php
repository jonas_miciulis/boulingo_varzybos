<?php

	include 'libraries/komanda.class.php';
	$customersObj = new komanda();
	
	include 'libraries/dalyvis.class.php';
	$dalyvisObj = new dalyvis();
	
	$formErrors = null;
	$fields = array();
	
	// nustatome privalomus laukus
	$required = array('id_Komanda','pavadinimas', 'komandos_nariu_skaicius', 'reitingas', 'komandos_telefono_nr');
	
	// maksimalūs leidžiami laukų ilgiai
	$maxLengths = array (
		'id_Komanda' => 20,
		'pavadinimas' => 20,
		'komandos_nariu_skaicius' => 1,
		'reitingas' => 4,
		'laimetu_turnyru_sk' => 2,
		'komandos_telefono_nr' => 9
	);
	
	// paspaustas išsaugojimo mygtukas
	if(!empty($_POST['submit'])) {
		
		// nustatome laukų validatorių tipus
		$validations = array (
			'id_Komanda' => 'positivenumber',
			'pavadinimas' => 'anything',
			'komandos_nariu_skaicius' => 'positivenumber',
			'reitingas' => 'positivenumber',
			'laimetu_turnyru_sk' => 'positivenumber',
			'komandos_telefono_nr' => 'positivenumber'
		);
		
		// sukuriame validatoriaus objektą
		include 'utils/validator.class.php';
		$validator = new validator($validations, $required, $maxLengths);

		if($validator->validate($_POST)) {
			// suformuojame laukų reikšmių masyvą SQL užklausai
			$dataN = $validator->preparePostFieldsForSQL();
			
			if(!empty($id)) {
				
				// redaguojame komanda
				$customersObj->updateKomanda($dataN);
				
				$dalyvisObj->deleteDalyvius($dataN);
				
				$dalyvisObj->insertDalyvis($dataN);
				
	
			
			} else {
				// įrašome naują dalyvi
				$customersObj->insertKomanda($dataN);
				$dalyvisObj->insertDalyvis($dataN);
			}

			// nukreipiame vartotoją į klientų puslapį
			header("Location: index.php?module={$module}");
			die();
		}
		else {                                         // čia papildyti
			// gauname klaidų pranešimą
			$formErrors = $validator->getErrorHTML();
			// laukų reikšmių kintamajam priskiriame įvestų laukų reikšmes
			$fields = $_POST;
			
			if(isset($_POST['vardai']) && sizeof($_POST['vardai']) > 0) {
				$i = 0;
				foreach($_POST['vardai'] as $key => $val) {
					$fields['dalyviu_list'][$i]['vardas'] = $val;
					$fields['dalyviu_list'][$i]['pavarde'] = $_POST['pavardes'][$key];
					$fields['dalyviu_list'][$i]['individ_reitingas'] = $_POST['individ_reitingai'][$key];
					$fields['dalyviu_list'][$i]['vid_tasku_skaicius'] = $_POST['vid_tasku_skaiciai'][$key];
					$fields['dalyviu_list'][$i]['rekordas'] = $_POST['rekordai'][$key];
					$fields['dalyviu_list'][$i]['laimetu_turnyru_sk'] = $_POST['laimetu_turnyru_skaiciai'][$key];
					$fields['dalyviu_list'][$i]['id_Dalyvis'] = $_POST['id_Dalyviai'][$key];
					$fields['dalyviu_list'][$i]['neaktyvus'] = $_POST['neaktyvus'][$key];
					$i++;
					echo "'{$val}'";
				}
			}
		}
	} else {
		// tikriname, ar nurodytas elemento id. Jeigu taip, išrenkame elemento duomenis ir jais užpildome formos laukus.
		if(!empty($id)) {
			// išrenkame klientą
			$fields = $customersObj->getKomanda($id);
					
		//	$fields['dalyviu_list'][] = $dalyvisObj->getSaskaitaListByID($fields['nr']);
		
	//	$saskaitos = $dalyvisObj->getSaskaitaListByID($fields['nr']);
	//	foreach($saskaitos as $key => $val) {
			
			$tmp = $dalyvisObj->getDalyvisListByID($id);
			
			if(sizeof($tmp) > 0) {
				foreach($tmp as $key => $val) {
//					$val['neaktyvus'] = 1;
					$fields['dalyviu_list'][] = $val;
				}
			}
			
	//		foreach($fields['dalyviu_list'] as $key => $val) {
	//			echo "'{$fields['nr']}'";
	//			echo "'{$val['suma']}'";
	//		}
			
			
			$fields['editing'] = 1;
		}
	}
?>
<ul id="pagePath">
	<li><a href="index.php">Pradžia</a></li>
	<li><a href="index.php?module=<?php echo $module; ?>">Komandos</a></li>
	<li><?php if(!empty($id)) echo "Komandos redagavimas"; else echo "Nauja komanda"; ?></li>
</ul>
<div class="float-clear"></div>
<div id="formContainer">
	<?php if($formErrors != null) { ?>
		<div class="errorBox">
			Neįvesti arba neteisingai įvesti šie laukai:
			<?php 
				echo $formErrors;
			?>
		</div>
	<?php } ?>
	<form action="" method="post">
		<fieldset>
			<legend>Komandos informacija</legend>
			<p>
				<label class="field" for="id_Komanda">Komandos ID<?php echo in_array('id_Komanda', $required) ? '<span> *</span>' : ''; ?></label>
					<?php if(!isset($fields['editing'])) { ?>
						<input type="text" id="id_Komanda" name="id_Komanda" class="textbox-150" value="<?php echo isset($fields['id_Komanda']) ? $fields['id_Komanda'] : ''; ?>" />
						<?php if(key_exists('id_Komanda', $maxLengths)) echo "<span class='max-len'>(iki {$maxLengths['id_Komanda']} simb.)</span>"; ?>
					<?php } else { ?>
						<span class="input-value"><?php echo $fields['id_Komanda']; ?></span>
						<input type="hidden" name="editing" value="1" />
						<input type="hidden" name="id_Komanda" value="<?php echo $fields['id_Komanda']; ?>" />
					<?php } ?>
			</p>
			<p>
				<label class="field" for="pavadinimas">Komandos pavadinimas<?php echo in_array('pavadinimas', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="pavadinimas" name="pavadinimas" class="textbox-150" value="<?php echo isset($fields['pavadinimas']) ? $fields['pavadinimas'] : ''; ?>" />
				<?php if(key_exists('pavadinimas', $maxLengths)) echo "<span class='max-len'>(iki {$maxLengths['pavadinimas']} simb.)</span>"; ?>
			</p>
			<p>
				<label class="field" for="komandos_nariu_skaicius">Komandos narių skaičius<?php echo in_array('komandos_nariu_skaicius', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="komandos_nariu_skaicius" name="komandos_nariu_skaicius" class="textbox-150" value="<?php echo isset($fields['komandos_nariu_skaicius']) ? $fields['komandos_nariu_skaicius'] : ''; ?>" />
				<?php if(key_exists('komandos_nariu_skaicius', $maxLengths)) echo "<span class='max-len'>(iki {$maxLengths['komandos_nariu_skaicius']} simb.)</span>"; ?>
			</p>
			<p>
				<label class="field" for="reitingas">Reitingas<?php echo in_array('reitingas', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="reitingas" name="reitingas" class="textbox-150" value="<?php echo isset($fields['reitingas']) ? $fields['reitingas'] : ''; ?>" />
				<?php if(key_exists('reitingas', $maxLengths)) echo "<span class='max-len'>(iki {$maxLengths['reitingas']} simb.)</span>"; ?>
			</p>
			<p>
				<label class="field" for="laimetu_turnyru_sk">Laimėtų turnyrų skaičius<?php echo in_array('laimetu_turnyru_sk', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="laimetu_turnyru_sk" name="laimetu_turnyru_sk" class="textbox-150" value="<?php echo isset($fields['laimetu_turnyru_sk']) ? $fields['laimetu_turnyru_sk'] : ''; ?>" />
				<?php if(key_exists('laimetu_turnyru_sk', $maxLengths)) echo "<span class='max-len'>(iki {$maxLengths['laimetu_turnyru_sk']} simb.)</span>"; ?>
			</p>
			<p>
				<label class="field" for="komandos_telefono_nr">Komandos telefono numeris<?php echo in_array('komandos_telefono_nr', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="komandos_telefono_nr" name="komandos_telefono_nr" class="textbox-150" value="<?php echo isset($fields['komandos_telefono_nr']) ? $fields['komandos_telefono_nr'] : ''; ?>" />
				<?php if(key_exists('komandos_telefono_nr', $maxLengths)) echo "<span class='max-len'>(iki {$maxLengths['komandos_telefono_nr']} simb.)</span>"; ?>
			</p>
		</fieldset>
		
		
		
		
		<fieldset>
			<legend>Komandos nariai</legend>
			<div class="childRowContainer">
			<p>
                <div class="labelLeft class="field" for="vardas">Vardas<?php echo in_array('vardas', $required) ? '<span> *</span>' : ''; ?></div>
				<div class="labelRight class="field" for="pavarde">Pavardė<?php echo in_array('pavarde', $required) ? '<span> *</span>' : ''; ?></div>
				<div class="labelLeft class="field" for="individ_reitingas">Reitingas<?php echo in_array('individ_reitingas', $required) ? '<span> *</span>' : ''; ?></div>
				<div class="labelRight class="field" for="vid_tasku_skaicius">Vidurkis<?php echo in_array('vid_tasku_skaicius', $required) ? '<span> *</span>' : ''; ?></div>
				<div class="labelLeft class="field" for="rekordas">Rekordas<?php echo in_array('rekordas', $required) ? '<span> *</span>' : ''; ?></div>
			</p>
			<p>
				<div class="labelLeft class="field" for="laimetu_turnyru_sk">Taurių sk.<?php echo in_array('laimetu_turnyru_sk', $required) ? '<span> *</span>' : ''; ?></div>
				<div class="labelRight class="field" for="id_Dalyvis">Dalyvis ID<?php echo in_array('id_Dalyvis', $required) ? '<span> *</span>' : ''; ?></div>
			</p>
				<div class="float-clear"></div>
			<p>
			</p>
				
				<?php
					if(empty($fields['dalyviu_list']) || sizeof($fields['dalyviu_list']) == 0) {
				?>
					<div class="childRow hidden">
						<input type="text" name="vardai[]" value="" class="textbox-70" disabled="disabled" />
						<input type="text" name="pavardes[]" value="" class="textbox-70" disabled="disabled" />
						<input type="text" name="individ_reitingai[]" value="" class="textbox-70" disabled="disabled" />
						<input type="text" name="vid_tasku_skaiciai[]" value="" class="textbox-70" disabled="disabled" />
						<input type="text" name="rekordai[]" value="" class="textbox-70" disabled="disabled" />
						<input type="text" name="laimetu_turnyru_skaiciai[]" value="" class="textbox-70" disabled="disabled" />
						<input type="text" name="id_Dalyviai[]" value="" class="textbox-70" disabled="disabled" />
						<input type="hidden" class="isDisabledForEditing" name="neaktyvus[]" value="0" />
						<a href="#" title="" class="removeChild">šalinti</a>
					</div>
					<div class="float-clear"></div>
					
								<?php
					} else {
						foreach($fields['dalyviu_list'] as $key => $val) {
						?>
						<div class="childRow">
							<input type="text" name="vardai[]" value="<?php echo $val['vardas']; ?>" class="textbox-70"  />
							<input type="text" name="pavardes[]" value="<?php echo $val['pavarde']; ?>" class="textbox-70" />
							<input type="text" name="individ_reitingai[]" value="<?php echo $val['individ_reitingas']; ?>" class="textbox-70" />
							<input type="text" name="vid_tasku_skaiciai[]" value="<?php echo $val['vid_tasku_skaicius']; ?>" class="textbox-70" />
							<input type="text" name="rekordai[]" value="<?php echo $val['rekordas']; ?>" class="textbox-70" />
							<input type="text" name="laimetu_turnyru_skaiciai[]" value="<?php echo $val['laimetu_turnyru_sk']; ?>" class="textbox-70" />
							<input type="text" name="id_Dalyviai[]" value="<?php echo $val['id_Dalyvis']; ?>" class="textbox-70<?php if(isset($val['neaktyvus']) && $val['neaktyvus'] == 1) echo ' disabledInput'; ?>" />
							<input type="hidden" class="isDisabledForEditing" name="neaktyvus[]" value="<?php if(isset($val['neaktyvus']) && $val['neaktyvus'] == 1) echo "1"; else echo "0"; ?>" />
							<?php if(key_exists('id_Dalyvis', $maxLengths)) echo "<span class='max-len'>(iki {$maxLengths['id_Dalyvis']} simb.)</span>"; ?>
							<a href="#" title="" class="removeChild<?php if(isset($val['neaktyvus']) && $val['neaktyvus'] == 1) echo " hidden"; ?> <?php $dalyvisObj->deleteSas($val['id_Dalyvis']) ?> ">šalinti</a>
							
							
							
							</div>
							<div class="float-clear"></div>
						<?php	
						}
					}
					?>					
							</select>
			</p>
			</div>
			<p id="newItemButtonContainer">
				<a href="#" title="" class="addChild">Pridėti</a>
			</p>
		</fieldset>
		
		
		
		
		<p class="required-note">* pažymėtus laukus užpildyti privaloma</p>
		<p>
			<input type="submit" class="submit" name="submit" value="Išsaugoti">
		</p>
	</form>
</div>