<ul id="pagePath">
	<li><a href="index.php">Pradžia</a></li>
	<li>Ataskaitos</li>
</ul>
<div id="actions">
	
</div>
<div class="float-clear"></div>

<div class="page">
	<ul class="reportList">
		<li>
			<p>
				<a href="report.php?id=1" target="_blank" title="Komandų ataskaita">Komandų ataskaita</a>
			</p>
			<p>Komandų pagal pasirinktą reitingą ataskaita.</p>
		</li>
		<li>
			<p>
				<a href="report.php?id=2" target="_blank" title="Žaidėjų ataskaita">Žaidėjų ataskaita</a>
			</p>
			<p>Žaidėjų pagal pasirinktą reitingą ataskaita.</p>
		</li>
		<li>
			<p>
				<a href="report.php?id=3" target="_blank" title="Boulingo kamuolių ataskaita">Boulingo kamuolių ataskaita</a>
			</p>
			<p>Boulingo kamuoliai pagal pasirinktą kainą ataskaita.</p>
		</li>
	</ul>
</div>