<?php

	// sukuriame personalo klasės objektą
	include 'libraries/personalas.class.php';
	$brandsObj = new personalas();
	
	// sukuriame puslapiavimo klasės objektą
	include 'utils/paging.class.php';
	$paging = new paging(NUMBER_OF_ROWS_IN_PAGE);
	
	if(!empty($removeId)) {
		// patikriname, ar šalinama markė nepriskirta modeliui
	//	$count = $brandsObj->getModelCountOfBrand($removeId);
		
		$removeErrorParameter = '';
		if($count == 0) {
			// šaliname personalą
			$brandsObj->deletePersonalas($removeId);
		} else {
			// nepašalinome, nes personalas priskirta boulingo klubui, rodome klaidos pranešimą
			$removeErrorParameter = '$remove_error=1';
		}

		// nukreipiame į personalo puslapį
		header("Location: index.php?module={$module}{$removeErrorParameter}");
		die();
	}
?>
<ul id="pagePath">
	<li><a href="index.php">Pradžia</a></li>
	<li>Personalas</li>
</ul>
<div id="actions">
	<a href='index.php?module=<?php echo $module; ?>&action=new'>Naujas personalas</a>
</div>
<div class="float-clear"></div>

<?php if(isset($_GET['remove_error'])) { ?>
	<div class="errorBox">
		Personalas nebuvo pašalintas.
	</div>
<?php } ?>

<table>
	<tr>
		<th>Personalo ID</th>
		<th>Darbuotojų skaičius</th>
		<th>Vadybininko vardas</th>
		<th>Vadybininko pavardė</th>
		<th></th>
	</tr>
	<?php
		// suskaičiuojame bendrą įrašų kiekį
		$elementCount = $brandsObj->getPersonalasListCount();

		// suformuojame sąrašo puslapius
		$paging->process($elementCount, $pageId);

		// išrenkame nurodyto puslapio markes
		$data = $brandsObj->getPersonalasList($paging->size, $paging->first);

		// suformuojame lentelę
		foreach($data as $key => $val) {
			echo
				"<tr>"
					. "<td>{$val['id_Personalas']}</td>"
					. "<td>{$val['personalo_dydis']}</td>"
					. "<td>{$val['vadybininko_vardas']}</td>"
					. "<td>{$val['vadybininko_pavarde']}</td>"
					. "<td>"
						. "<a href='#' onclick='showConfirmDialog(\"{$module}\", \"{$val['id_Personalas']}\"); return false;' title=''>šalinti</a>&nbsp;"
						. "<a href='index.php?module={$module}&id={$val['id_Personalas']}' title=''>redaguoti</a>"
					. "</td>"
				. "</tr>";
		}
	?>
</table>

<?php
	// įtraukiame puslapių šabloną
	include 'controls/paging.php';
?>