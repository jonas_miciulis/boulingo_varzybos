﻿<?php
	
	include 'libraries/boulingo_klubas.class.php';
	$customersObj = new boulingo_klubas();

	$formErrors = null;
	$fields = array();
	
	// nustatome privalomus formos laukus
	$required = array('id_Boulingo_klubas', 'pavadinimas');
	
	// maksimalūs leidžiami laukų ilgiai
	$maxLengths = array (
		'id_Boulingo_klubas' => 20
	);
	
	// vartotojas paspaudė išsaugojimo mygtuką
	if(!empty($_POST['submit'])) {
		include 'utils/validator.class.php';
		
		// nustatome laukų validatorių tipus
		$validations = array (
			'id_Boulingo_klubas' => 'positivenumber',
			'pavadinimas' => 'alfanum'
		);
		
		// sukuriame laukų validatoriaus objektą
		$validator = new validator($validations, $required, $maxLengths);

		// laukai įvesti be klaidų
		if($validator->validate($_POST)) {
			// suformuojame laukų reikšmių masyvą SQL užklausai
			$data = $validator->preparePostFieldsForSQL();

			if(isset($data['editing'])) {
				// redaguojame klientą
				$customersObj->updateBoulingoKlubas($data);
			} else {
				// įrašome naują klientą
				$customersObj->insertBoulingoKlubas($data);
			}

			// nukreipiame vartotoją į klientų puslapį
			header("Location: index.php?module={$module}");
			die();
		}
		else {
			// gauname klaidų pranešimą
			$formErrors = $validator->getErrorHTML();
			// laukų reikšmių kintamajam priskiriame įvestų laukų reikšmes
			$fields = $_POST;
		}
	} else {
		// tikriname, ar nurodytas elemento id. Jeigu taip, išrenkame elemento duomenis ir jais užpildome formos laukus.
		if(!empty($id)) {
			// išrenkame klientą
			$fields = $customersObj->getBoulingoKlubas($id);
			$fields['editing'] = 1;
		}
	}
?>
<ul id="pagePath">
	<li><a href="index.php">Pradžia</a></li>
	<li><a href="index.php?module=<?php echo $module; ?>">Boulingo klubai</a></li>
	<li><?php if(!empty($id)) echo "Klubo redagavimas"; else echo "Naujas klubas"; ?></li>
</ul>
<div class="float-clear"></div>
<div id="formContainer">
	<?php if($formErrors != null) { ?>
		<div class="errorBox">
			Neįvesti arba neteisingai įvesti šie laukai:
			<?php 
				echo $formErrors;
			?>
		</div>
	<?php } ?>
	<form action="" method="post">
		<fieldset>
			<legend>Boulingo klubo informacija</legend>
				<p>
					<label class="field" for="id_Boulingo_klubas">Boulingo klubo ID<?php echo in_array('id_Boulingo_klubas', $required) ? '<span> *</span>' : ''; ?></label>
					<?php if(!isset($fields['editing'])) { ?>
						<input type="text" id="id_Boulingo_klubas" name="id_Boulingo_klubas" class="textbox-150" value="<?php echo isset($fields['id_Boulingo_klubas']) ? $fields['id_Boulingo_klubas'] : ''; ?>" />
						<?php if(key_exists('id_Boulingo_klubas', $maxLengths)) echo "<span class='max-len'>(iki {$maxLengths['id_Boulingo_klubas']} simb.)</span>"; ?>
					<?php } else { ?>
						<span class="input-value"><?php echo $fields['id_Boulingo_klubas']; ?></span>
						<input type="hidden" name="editing" value="1" />
						<input type="hidden" name="id_Boulingo_klubas" value="<?php echo $fields['id_Boulingo_klubas']; ?>" />
					<?php } ?>
				</p>
			<p>
			<p>
				<label class="field" for="pavadinimas">Boulingo klubo pavadinimas<?php echo in_array('pavadinimas', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="pavadinimas" name="pavadinimas" class="textbox-150" value="<?php echo isset($fields['pavadinimas']) ? $fields['pavadinimas'] : ''; ?>" />
				<?php if(key_exists('pavadinimas', $maxLengths)) echo "<span class='max-len'>(iki {$maxLengths['pavadinimas']} simb.)</span>"; ?>
			</p>
			
			<p>
				<label class="field" for="taku_sk">Takų skaičius<?php echo in_array('taku_sk', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="taku_sk" name="taku_sk" class="textbox-150" value="<?php echo isset($fields['taku_sk']) ? $fields['taku_sk'] : ''; ?>" />
			</p>
			
			<p>
				<label class="field" for="takelio_kaina_valandai">Tako kaina valandai<?php echo in_array('takelio_kaina_valandai', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="takelio_kaina_valandai" name="takelio_kaina_valandai" class="textbox-150" value="<?php echo isset($fields['takelio_kaina_valandai']) ? $fields['takelio_kaina_valandai'] : ''; ?>" />
				<?php if(key_exists('takelio_kaina_valandai', $maxLengths)) echo "<span class='max-len'>(iki {$maxLengths['takelio_kaina_valandai']} simb.)</span>"; ?>
			</p>
		</fieldset>
		
		<fieldset>
			<legend>Kontaktai</legend>
			<p>
				<label class="field" for="telefono_numeris">Telefono numeris<?php echo in_array('telefono_numeris', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="telefono_numeris" name="telefono_numeris" class="textbox-150" value="<?php echo isset($fields['telefono_numeris']) ? $fields['telefono_numeris'] : ''; ?>" />
			</p>
			<p>
				<label class="field" for="elektroninis_pastas">Elektroninis paštas<?php echo in_array('elektroninis_pastas', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="elektroninis_pastas" name="elektroninis_pastas" class="textbox-150" value="<?php echo isset($fields['elektroninis_pastas']) ? $fields['elektroninis_pastas'] : ''; ?>" />
			</p>
			<p>
				<label class="field" for="adresas">Adresas<?php echo in_array('adresas', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="adresas" name="adresas" class="textbox-150" value="<?php echo isset($fields['adresas']) ? $fields['adresas'] : ''; ?>" />
			</p>
		</fieldset>
		
		<fieldset>
			<legend>Personalas</legend>
			<p>
				<label class="field" for="personalo_dydis">Personalo dydis<?php echo in_array('personalo_dydis', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="personalo_dydis" name="personalo_dydis" class="textbox-150" value="<?php echo isset($fields['personalo_dydis']) ? $fields['personalo_dydis'] : ''; ?>" />
			</p>
			<p>
				<label class="field" for="vadybininko_vardas">Vadybininko vardas<?php echo in_array('vadybininko_vardas', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="vadybininko_vardas" name="vadybininko_vardas" class="textbox-150" value="<?php echo isset($fields['vadybininko_vardas']) ? $fields['vadybininko_vardas'] : ''; ?>" />
			</p>
			<p>
				<label class="field" for="vadybininko_pavarde">Vadybininko pavardė<?php echo in_array('vadybininko_pavarde', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="vadybininko_pavarde" name="vadybininko_pavarde" class="textbox-150" value="<?php echo isset($fields['vadybininko_pavarde']) ? $fields['vadybininko_pavarde'] : ''; ?>" />
			</p>
		</fieldset>
		
		<p class="required-note">* pažymėtus laukus užpildyti privaloma</p>
		<p>
			<input type="submit" class="submit" name="submit" value="Išsaugoti">
		</p>
	</form>
</div>