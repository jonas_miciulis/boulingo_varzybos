<?php
	
	include 'libraries/varzybos.class.php';
	$brandsObj = new varzybos();

	include 'libraries/registracija.class.php';
	$customersObj = new registracija();
	
	$formErrors = null;
	$fields = array();
	
	// nustatome privalomus laukus
	$required = array('id_Registracija', 'fk_Varzybosid_Varzybos', 'startinis_mokestis', 'reg_pradzios_data', 'reg_pabaigos_data');
	
	// maksimalūs leidžiami laukų ilgiai
	$maxLengths = array (
		'id_Registracija' => 20
	);
	
	// vartotojas paspaudė išsaugojimo mygtuką
	if(!empty($_POST['submit'])) {
		include 'utils/validator.class.php';
		
		// nustatome laukų validatorių tipus
		$validations = array (
			'id_Registracija' => 'positivenumber',
			'fk_Varzybosid_Varzybos' => 'positivenumber',
			'startinis_mokestis' => 'positivenumber',
			'reg_pradzios_data' => 'date',
			'reg_pabaigos_data' => 'date');
		
		// sukuriame laukų validatoriaus objektą
		$validator = new validator($validations, $required, $maxLengths);

		// laukai įvesti be klaidų
		if($validator->validate($_POST)) {
			// suformuojame laukų reikšmių masyvą SQL užklausai
			$dataN = $validator->preparePostFieldsForSQL();
			
			
			
			if(isset($dataN['editing'])) {
				// redaguojame registraciją
				$customersObj->updateRegistracija($dataN);
			} else {
				// įrašome naują registraciją
				$customersObj->insertRegistracija($dataN);
			}

			// nukreipiame vartotoją į klientų puslapį
			header("Location: index.php?module={$module}");
			die();
		}
		else {
			// gauname klaidų pranešimą
			$formErrors = $validator->getErrorHTML();
			// laukų reikšmių kintamajam priskiriame įvestų laukų reikšmes
			$fields = $_POST;
		}
	} else {
		// tikriname, ar nurodytas elemento id. Jeigu taip, išrenkame elemento duomenis ir jais užpildome formos laukus.
		if(!empty($id)) {
			// išrenkame registraciją
			$fields = $customersObj->getRegistracija($id);
			$fields['editing'] = 1;
		}
	}
?>
<ul id="pagePath">
	<li><a href="index.php">Pradžia</a></li>
	<li><a href="index.php?module=<?php echo $module; ?>">Registracija</a></li>
	<li><?php if(!empty($id)) echo "Registracijos redagavimas"; else echo "Nauja registracija"; ?></li>
</ul>
<div class="float-clear"></div>
<div id="formContainer">
	<?php if($formErrors != null) { ?>
		<div class="errorBox">
			Neįvesti arba neteisingai įvesti šie laukai:
			<?php 
				echo $formErrors;
			?>
		</div>
	<?php } ?>
	<form action="" method="post">
		<fieldset>
			<legend>Registracijos informacija</legend>
			<p>
				<label class="field" for="reg_pradzios_data">Registracijos pradžia<?php echo in_array('reg_pradzios_data', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="reg_pradzios_data" name="reg_pradzios_data" class="date textbox-70" value="<?php echo isset($fields['reg_pradzios_data']) ? $fields['reg_pradzios_data'] : ''; ?>">
			</p>
			<p>
				<label class="field" for="reg_pabaigos_data">Registracijos pabaiga<?php echo in_array('reg_pabaigos_data', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="reg_pabaigos_data" name="reg_pabaigos_data" class="date textbox-70" value="<?php echo isset($fields['reg_pabaigos_data']) ? $fields['reg_pabaigos_data'] : ''; ?>">
			</p>
			<p>
				<label class="field" for="min_komandu_sk">Min komandų kiekis<?php echo in_array('min_komandu_sk', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="min_komandu_sk" name="min_komandu_sk" class="textbox-70" value="<?php echo isset($fields['min_komandu_sk']) ? $fields['min_komandu_sk'] : ''; ?>">
			</p>
			<p>
				<label class="field" for="max_komandu_sk">Max komandų kiekis<?php echo in_array('max_komandu_sk', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="max_komandu_sk" name="max_komandu_sk" class="textbox-70" value="<?php echo isset($fields['max_komandu_sk']) ? $fields['max_komandu_sk'] : ''; ?>">
			</p>
			<p>
				<label class="field" for="startinis_mokestis">Startinis mokestis<?php echo in_array('startinis_mokestis', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="startinis_mokestis" name="startinis_mokestis" class="textbox-150" value="<?php echo isset($fields['startinis_mokestis']) ? $fields['startinis_mokestis'] : ''; ?>">
			</p>
			
			<p>
				<?php if(!isset($fields['editing'])) { ?>
					<label class="field" for="id_Registracija">Registracijos ID<?php echo in_array('id_Registracija', $required) ? '<span> *</span>' : ''; ?></label>
					<input type="text" id="id_Registracija" name="id_Registracija" class="textbox-70" value="<?php echo isset($fields['id_Registracija']) ? $fields['id_Registracija'] : ''; ?>">
					<?php if(key_exists('id_Registracija', $maxLengths)) echo "<span class='max-len'>(iki {$maxLengths['id_Registracija']} simb.)</span>"; ?>
				<?php } else { ?>
						<label class="field" for="id_Registracija">Registracijos ID</label>
						<span class="input-value"><?php echo $fields['id_Registracija']; ?></span>
						<input type="hidden" name="editing" value="1" />
						<input type="hidden" name="id_Registracija" value="<?php echo $fields['id_Registracija']; ?>" />
				<?php } ?>
			</p>
			
			<p>
				<label class="field" for="brand">Varžybos<?php echo in_array('fk_Varzybosid_Varzybos', $required) ? '<span> *</span>' : ''; ?></label>
				<select id="brand" name="fk_Varzybosid_Varzybos">
					<option value="-1">--------</option>
					<?php
						// išrenkame visas ---
						$brands = $brandsObj->getVarzybosList();
						foreach($brands as $key => $val) {
							$selected = "";
							if(isset($fields['fk_Varzybosid_Varzybos']) && $fields['fk_Varzybosid_Varzybos'] == $val['id_Varzybos']) {
								$selected = " selected='selected'";
							}
							echo "<option{$selected} value='{$val['id_Varzybos']}'>{$val['pavadinimas']}</option>";
						}
					?>
				</select>
			</p>

			
		</fieldset>
		<p class="required-note">* pažymėtus laukus užpildyti privaloma</p>
		<p>
			<input type="submit" class="submit" name="submit" value="Išsaugoti">
		</p>
	</form>
</div>