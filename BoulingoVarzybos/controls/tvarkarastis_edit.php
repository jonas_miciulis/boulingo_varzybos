<?php
	
	include 'libraries/varzybos.class.php';
	$brandsObj = new varzybos();

	include 'libraries/tvarkarastis.class.php';
	$customersObj = new tvarkarastis();
	
	$formErrors = null;
	$fields = array();
	
	// nustatome privalomus laukus
	$required = array('id_Tvarkarastis', 'fk_Varzybosid_Varzybos', 'kovos_data');
	
	// maksimalūs leidžiami laukų ilgiai
	$maxLengths = array (
		'id_Tvarkarastis' => 20
	);
	
	// vartotojas paspaudė išsaugojimo mygtuką
	if(!empty($_POST['submit'])) {
		include 'utils/validator.class.php';
		
		// nustatome laukų validatorių tipus
		$validations = array (
			'id_Tvarkarastis' => 'positivenumber',
			'kovos_data' => 'date',
			'fk_Varzybosid_Varzybos' => 'positivenumber');
		
		// sukuriame laukų validatoriaus objektą
		$validator = new validator($validations, $required, $maxLengths);

		// laukai įvesti be klaidų
		if($validator->validate($_POST)) {
			// suformuojame laukų reikšmių masyvą SQL užklausai
			$dataN = $validator->preparePostFieldsForSQL();
			
			
			
			if(isset($dataN['editing'])) {
				// redaguojame klientą
				$customersObj->updateTvarkarastis($dataN);
			} else {
				// įrašome naują klientą
				$customersObj->insertTvarkarastis($dataN);
			}

			// nukreipiame vartotoją į klientų puslapį
			header("Location: index.php?module={$module}");
			die();
		}
		else {
			// gauname klaidų pranešimą
			$formErrors = $validator->getErrorHTML();
			// laukų reikšmių kintamajam priskiriame įvestų laukų reikšmes
			$fields = $_POST;
		}
	} else {
		// tikriname, ar nurodytas elemento id. Jeigu taip, išrenkame elemento duomenis ir jais užpildome formos laukus.
		if(!empty($id)) {
			// išrenkame tvarkaraštį
			$fields = $customersObj->getTvarkarastis($id);
			$fields['editing'] = 1;
		}
	}
?>
<ul id="pagePath">
	<li><a href="index.php">Pradžia</a></li>
	<li><a href="index.php?module=<?php echo $module; ?>">Tvarkaraštis</a></li>
	<li><?php if(!empty($id)) echo "Kliento redagavimas"; else echo "Naujas tvarkaraštis"; ?></li>
</ul>
<div class="float-clear"></div>
<div id="formContainer">
	<?php if($formErrors != null) { ?>
		<div class="errorBox">
			Neįvesti arba neteisingai įvesti šie laukai:
			<?php 
				echo $formErrors;
			?>
		</div>
	<?php } ?>
	<form action="" method="post">
		<fieldset>
			<legend>Tvarkaraščio informacija</legend>
			<p>
				<label class="field" for="kovos_data">Kovos data<?php echo in_array('kovos_data', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="kovos_data" name="kovos_data" class="date textbox-70" value="<?php echo isset($fields['kovos_data']) ? $fields['kovos_data'] : ''; ?>">
			</p>
			<p>
				<label class="field" for="kovos_laikas">Kovos laikas<?php echo in_array('kovos_laikas', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="kovos_laikas" name="kovos_laikas" class="textbox-150" value="<?php echo isset($fields['kovos_laikas']) ? $fields['kovos_laikas'] : ''; ?>">
			</p>
			
			<p>
				<?php if(!isset($fields['editing'])) { ?>
					<label class="field" for="id_Tvarkarastis">Tvarkaraščio ID<?php echo in_array('id_Tvarkarastis', $required) ? '<span> *</span>' : ''; ?></label>
					<input type="text" id="id_Tvarkarastis" name="id_Tvarkarastis" class="textbox-70" value="<?php echo isset($fields['id_Tvarkarastis']) ? $fields['id_Tvarkarastis'] : ''; ?>">
					<?php if(key_exists('id_Tvarkarastis', $maxLengths)) echo "<span class='max-len'>(iki {$maxLengths['id_Tvarkarastis']} simb.)</span>"; ?>
				<?php } else { ?>
						<label class="field" for="id_Tvarkarastis">Tvarkaraščio ID</label>
						<span class="input-value"><?php echo $fields['id_Tvarkarastis']; ?></span>
						<input type="hidden" name="editing" value="1" />
						<input type="hidden" name="id_Tvarkarastis" value="<?php echo $fields['id_Tvarkarastis']; ?>" />
				<?php } ?>
			</p>
			
			<p>
				<label class="field" for="brand">Varžybos<?php echo in_array('fk_Varzybosid_Varzybos', $required) ? '<span> *</span>' : ''; ?></label>
				<select id="brand" name="fk_Varzybosid_Varzybos">
					<option value="-1">--------</option>
					<?php
						// išrenkame visas ---
						$brands = $brandsObj->getVarzybosList();
						foreach($brands as $key => $val) {
							$selected = "";
							if(isset($fields['fk_Varzybosid_Varzybos']) && $fields['fk_Varzybosid_Varzybos'] == $val['id_Varzybos']) {
								$selected = " selected='selected'";
							}
							echo "<option{$selected} value='{$val['id_Varzybos']}'>{$val['pavadinimas']}</option>";
						}
					?>
				</select>
			</p>

			
		</fieldset>
		<p class="required-note">* pažymėtus laukus užpildyti privaloma</p>
		<p>
			<input type="submit" class="submit" name="submit" value="Išsaugoti">
		</p>
	</form>
</div>