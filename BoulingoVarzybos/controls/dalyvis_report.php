<?php
include 'libraries/dalyvis.class.php';
	$servicesObj = new dalyvis();
	
	$formErrors = null;
	$fields = array();
	$formSubmitted = false;
		
	$data = array();
	if(!empty($_POST['submit'])) {
		$formSubmitted = true;

		// nustatome laukų validatorių tipus
		$validations = array (
			'dataNuo' => 'positivenumber',
			'dataIki' => 'positivenumber');
		
		// sukuriame validatoriaus objektą
		include 'utils/validator.class.php';
		$validator = new validator($validations);
		

		if($validator->validate($_POST)) {
			// suformuojame laukų reikšmių masyvą SQL užklausai
			$data = $validator->preparePostFieldsForSQL();
		} else {
			// gauname klaidų pranešimą
			$formErrors = $validator->getErrorHTML();
			// gauname įvestus laukus
			$fields = $_POST;
		}
	}
	
if($formSubmitted == true && ($formErrors == null)) { ?>
	<div id="header">
		<ul id="reportInfo">
			<li class="title">II ataskaita</li>
			<li>Sudarymo data: <span><?php echo date("Y-m-d"); ?></span></li>
			<li>Žaidėjo reitingo reziai:
				<span>
					<?php
						if(!empty($data['dataNuo'])) {
							if(!empty($data['dataIki'])) {
								echo "nuo {$data['dataNuo']} iki {$data['dataIki']}";
							} else {
								echo "nuo {$data['dataNuo']}";
							}
						} else {
							if(!empty($data['dataIki'])) {
								echo "iki {$data['dataIki']}";
							} else {
								echo "nenurodyta";
							}
						}
					?>
				</span>
				<a href="report.php?id=1" title="Nauja ataskaita" class="newReport">nauja ataskaita</a>
			</li>
		</ul>
	</div>
<?php } ?>
<div id="content">
	<div id="contentMain">
		<?php if($formSubmitted == false || $formErrors != null) { ?>
			<div id="formContainer">
				<?php if($formErrors != null) { ?>
					<div class="errorBox">
						Neįvesti arba neteisingai įvesti šie laukai:
						<?php 
							echo $formErrors;
						?>
					</div>
				<?php } ?>
				<form action="" method="post">
					<fieldset>
						<legend>Įveskite ataskaitos kriterijus</legend>
						<p><label class="field" for="dataNuo">Žaidėjo reitingas nuo</label><input type="text" id="dataNuo" name="dataNuo" class="textbox-100" value="<?php echo isset($fields['dataNuo']) ? $fields['dataNuo'] : ''; ?>" /></p>
						<p><label class="field" for="dataIki">Žaidėjo reitingas iki</label><input type="text" id="dataIki" name="dataIki" class="textbox-100" value="<?php echo isset($fields['dataIki']) ? $fields['dataIki'] : ''; ?>" /></p>
					</fieldset>
					<p><input type="submit" class="submit" name="submit" value="Sudaryti ataskaitą"></p>
				</form>
			</div>
		<?php } else {
			

					// išrenkame ataskaitos duomenis
                                $salisData = $servicesObj->getDalyviuz($data['dataNuo'], $data['dataIki']);
								$servicesStats = $servicesObj->getStatsOfDalyviai($data['dataNuo'], $data['dataIki']);
								if(sizeof($salisData) > 0) { ?>
		
                                    <table class="reportTable">
                                        <tr>
                                            <th>Vardas</th>
                                            <th>Pavardė</th>
											<th>Atstovauja komandą</th>
                                            <th>Reitingas</th>
                                            <th>Rekordas</th>
                                        </tr>

                                        <?php

                                            // suformuojame lentelę
                                            foreach($salisData as $key=>$val){
                                                echo "<tr>"
                                                        . "<td>{$val['vardas']}</td>"
                                                        . "<td>{$val['pavarde']}</td>"
														. "<td>{$val['pavad']}</td>"
                                                        . "<td>{$val['individ_reitingas']}</td>"
                                                        . "<td>{$val['rekordas']}</td>"
                                                    . "</tr>";
                                            }
                                        ?>
										<tr class="aggregate">
											<td></td>
											<td></td>
											<td></td>
											<td class="label">Šiame reitinge rekordas yra:</td>
											<td class="border"><?php echo "{$servicesStats[0]['wrekordas']}"; ?></td>
										</tr>
                                    </table>
			<?php   } else { ?>
                                            <div class="warningBox">
                                                Nėra tokio reitingo žaidėjų!
                                            </div>
					<?php
					}
			} ?>
    </div>
</div>