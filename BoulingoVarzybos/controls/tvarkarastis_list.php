<?php
	// sukuriame modelių klasės objektą
	include 'libraries/tvarkarastis.class.php';
	$modelsObj = new tvarkarastis();
	
	// sukuriame puslapiavimo klasės objektą
	include 'utils/paging.class.php';
	$paging = new paging(NUMBER_OF_ROWS_IN_PAGE);
	
	if(!empty($removeId)) {
		// patikriname, ar šalinamas modelis nenaudojamas, t.y. nepriskirtas jokiam automobiliui
	//	$count = $modelsObj->getCarCountOfModel($removeId);
		
		$removeErrorParameter = '';
		if($count == 0) {
			// pašaliname modelį
			$modelsObj->deleteTvarkarastis($removeId);
		} else {
			// nepašalinome, nes modelis priskirtas bent vienam automobiliui, rodome klaidos pranešimą
			$removeErrorParameter = '&remove_error=1';
		}
		
		// nukreipiame į modelių puslapį
		header("Location: index.php?module={$module}{$removeErrorParameter}");
		die();
	}
?>
<ul id="pagePath">
	<li><a href="index.php">Pradžia</a></li>
	<li>Boulingo varžybų tvarkaraščiai</li>
</ul>
<div id="actions">
	<a href='index.php?module=<?php echo $module; ?>&action=new'>Naujas tvarkaraštis</a>
</div>
<div class="float-clear"></div>

<?php if(isset($_GET['remove_error'])) { ?>
	<div class="errorBox">
		Tvarkaraštis nebuvo pašalintas.
	</div>
<?php } ?>

<table>
	<tr>
		<th>Tvarkaraščio ID</th>
		<th>Kovos data</th>
		<th>Kovos laikas</th>
		<th></th>
	</tr>
	<?php
		// suskaičiuojame bendrą įrašų kiekį
		$elementCount = $modelsObj->getTvarkarastisListCount();

		// suformuojame sąrašo puslapius
		$paging->process($elementCount, $pageId);

		// išrenkame nurodyto puslapio modelius
		$data = $modelsObj->getTvarkarastisList($paging->size, $paging->first);

		// suformuojame lentelę
		foreach($data as $key => $val) {
			echo
				"<tr>"
					. "<td>{$val['id_Tvarkarastis']}</td>"
					. "<td>{$val['kovos_data']}</td>"
					. "<td>{$val['kovos_laikas']}</td>"
					. "<td>"
						. "<a href='#' onclick='showConfirmDialog(\"{$module}\", \"{$val['id_Tvarkarastis']}\"); return false;' title=''>šalinti</a>&nbsp;"
						. "<a href='index.php?module={$module}&id={$val['id_Tvarkarastis']}' title=''>redaguoti</a>"
					. "</td>"
				. "</tr>";
		}
	?>
</table>

<?php
	// įtraukiame puslapių šabloną
	include 'controls/paging.php';
?>