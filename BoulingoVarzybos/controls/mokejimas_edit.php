<?php
	include 'libraries/komanda.class.php';
	$brandsObj = new komanda();
	
	include 'libraries/registracija.class.php';
	$brands2Obj = new registracija();

	include 'libraries/mokejimas.class.php';
	$customersObj = new mokejimas();
	
	$formErrors = null;
	$fields = array();
	
	// nustatome privalomus laukus
	$required = array('id_Mokejimas', 'fk_Komandaid_Komanda', 'fk_Registracijaid_Registracija', 'pervedama_suma', 'data');
	
	// maksimalūs leidžiami laukų ilgiai
	$maxLengths = array (
		'id_Mokejimas' => 20
	);
	
	// vartotojas paspaudė išsaugojimo mygtuką
	if(!empty($_POST['submit'])) {
		include 'utils/validator.class.php';
		
		// nustatome laukų validatorių tipus
		$validations = array (
			'id_Mokejimas' => 'positivenumber',
			'data' => 'date',
			'fk_Registracijaid_Registracija' => 'positivenumber',
			'pervedama_suma' => 'positivenumber',
			'fk_Komandaid_Komanda' => 'positivenumber');
		
		// sukuriame laukų validatoriaus objektą
		$validator = new validator($validations, $required, $maxLengths);

		// laukai įvesti be klaidų
		if($validator->validate($_POST)) {
			// suformuojame laukų reikšmių masyvą SQL užklausai
			$dataN = $validator->preparePostFieldsForSQL();
			
			
			
			if(isset($dataN['editing'])) {
				// redaguojame klientą
				$customersObj->updateMokejimas($dataN);
			} else {
				// įrašome naują klientą
				$customersObj->insertMokejimas($dataN);
			}

			// nukreipiame vartotoją į klientų puslapį
			header("Location: index.php?module={$module}");
			die();
		}
		else {
			// gauname klaidų pranešimą
			$formErrors = $validator->getErrorHTML();
			// laukų reikšmių kintamajam priskiriame įvestų laukų reikšmes
			$fields = $_POST;
		}
	} else {
		// tikriname, ar nurodytas elemento id. Jeigu taip, išrenkame elemento duomenis ir jais užpildome formos laukus.
		if(!empty($id)) {
			// išrenkame tvarkaraštį
			$fields = $customersObj->getMokejimas($id);
			$fields['editing'] = 1;
		}
	}
?>
<ul id="pagePath">
	<li><a href="index.php">Pradžia</a></li>
	<li><a href="index.php?module=<?php echo $module; ?>">Mokėjimas</a></li>
	<li><?php if(!empty($id)) echo "Mokėjimo redagavimas"; else echo "Naujas mokėjimas"; ?></li>
</ul>
<div class="float-clear"></div>
<div id="formContainer">
	<?php if($formErrors != null) { ?>
		<div class="errorBox">
			Neįvesti arba neteisingai įvesti šie laukai:
			<?php 
				echo $formErrors;
			?>
		</div>
	<?php } ?>
	<form action="" method="post">
		<fieldset>
			<legend>Mokesčio informacija</legend>
			<p>
				<label class="field" for="data">Mokėjimo data<?php echo in_array('data', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="data" name="data" class="date textbox-70" value="<?php echo isset($fields['data']) ? $fields['data'] : ''; ?>">
			</p>
			<p>
				<label class="field" for="pervedama_suma">Pervedama suma<?php echo in_array('pervedama_suma', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="pervedama_suma" name="pervedama_suma" class="textbox-150" value="<?php echo isset($fields['pervedama_suma']) ? $fields['pervedama_suma'] : ''; ?>">
			</p>
			
			<p>
				<?php if(!isset($fields['editing'])) { ?>
					<label class="field" for="id_Mokejimas">Mokėjimo ID<?php echo in_array('id_Mokejimas', $required) ? '<span> *</span>' : ''; ?></label>
					<input type="text" id="id_Mokejimas" name="id_Mokejimas" class="textbox-70" value="<?php echo isset($fields['id_Mokejimas']) ? $fields['id_Mokejimas'] : ''; ?>">
					<?php if(key_exists('id_Mokejimas', $maxLengths)) echo "<span class='max-len'>(iki {$maxLengths['id_Mokejimas']} simb.)</span>"; ?>
				<?php } else { ?>
						<label class="field" for="id_Mokejimas">Mokėjimo ID</label>
						<span class="input-value"><?php echo $fields['id_Mokejimas']; ?></span>
						<input type="hidden" name="editing" value="1" />
						<input type="hidden" name="id_Mokejimas" value="<?php echo $fields['id_Mokejimas']; ?>" />
				<?php } ?>
			</p>
			
			<p>
				<label class="field" for="brand">Komanda<?php echo in_array('fk_Komandaid_Komanda', $required) ? '<span> *</span>' : ''; ?></label>
				<select id="brand" name="fk_Komandaid_Komanda">
					<option value="-1">--------</option>
					<?php
						// išrenkame visas ---
						$brands = $brandsObj->getKomandaList();
						foreach($brands as $key => $val) {
							$selected = "";
							if(isset($fields['fk_Komandaid_Komanda']) && $fields['fk_Komandaid_Komanda'] == $val['id_Komanda']) {
								$selected = " selected='selected'";
							}
							echo "<option{$selected} value='{$val['id_Komanda']}'>{$val['pavadinimas']}</option>";
						}
					?>
				</select>
			</p>
			
			<p>
				<label class="field" for="brand">Registracija<?php echo in_array('fk_Registracijaid_Registracija', $required) ? '<span> *</span>' : ''; ?></label>
				<select id="brand" name="fk_Registracijaid_Registracija">
					<option value="-1">--------</option>
					<?php
						// išrenkame visas ---
						$brands2 = $brands2Obj->getRegistracijaList();
						foreach($brands2 as $key => $val) {
							$selected = "";
							if(isset($fields['fk_Registracijaid_Registracija']) && $fields['fk_Registracijaid_Registracija'] == $val['id_Registracija']) {
								$selected = " selected='selected'";
							}
							echo "<option{$selected} value='{$val['id_Registracija']}'>{$val['id_Registracija']}</option>";
						}
					?>
				</select>
			</p>

			
		</fieldset>
		<p class="required-note">* pažymėtus laukus užpildyti privaloma</p>
		<p>
			<input type="submit" class="submit" name="submit" value="Išsaugoti">
		</p>
	</form>
</div>