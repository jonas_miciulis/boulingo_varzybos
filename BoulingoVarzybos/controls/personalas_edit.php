<?php
	
	include 'libraries/boulingo_klubas.class.php';
	$brandsObj = new boulingo_klubas();

	include 'libraries/personalas.class.php';
	$customersObj = new personalas();
	
	$formErrors = null;
	$fields = array();
	
	// nustatome privalomus laukus
	$required = array('id_Personalas', 'fk_Boulingo_klubasid_Boulingo_klubas');
	
	// maksimalūs leidžiami laukų ilgiai
	$maxLengths = array (
		'id_Personalas' => 20
	);
	
	// vartotojas paspaudė išsaugojimo mygtuką
	if(!empty($_POST['submit'])) {
		include 'utils/validator.class.php';
		
		// nustatome laukų validatorių tipus
		$validations = array (
			'id_Personalas' => 'positivenumber',
			'fk_Boulingo_klubasid_Boulingo_klubas' => 'positivenumber'
			);
		
		// sukuriame laukų validatoriaus objektą
		$validator = new validator($validations, $required, $maxLengths);

		// laukai įvesti be klaidų
		if($validator->validate($_POST)) {
			// suformuojame laukų reikšmių masyvą SQL užklausai
			$dataN = $validator->preparePostFieldsForSQL();
			
			
			
			if(isset($dataN['editing'])) {
				// redaguojame registraciją
				$customersObj->updatePersonalas($dataN);
			} else {
				// įrašome naują registraciją
				$customersObj->insertPersonalas($dataN);
			}

			// nukreipiame vartotoją į klientų puslapį
			header("Location: index.php?module={$module}");
			die();
		}
		else {
			// gauname klaidų pranešimą
			$formErrors = $validator->getErrorHTML();
			// laukų reikšmių kintamajam priskiriame įvestų laukų reikšmes
			$fields = $_POST;
		}
	} else {
		// tikriname, ar nurodytas elemento id. Jeigu taip, išrenkame elemento duomenis ir jais užpildome formos laukus.
		if(!empty($id)) {
			// išrenkame registraciją
			$fields = $customersObj->getPersonalas($id);
			$fields['editing'] = 1;
		}
	}
?>
<ul id="pagePath">
	<li><a href="index.php">Pradžia</a></li>
	<li><a href="index.php?module=<?php echo $module; ?>">Personalas</a></li>
	<li><?php if(!empty($id)) echo "Personalo redagavimas"; else echo "Naujas personalas"; ?></li>
</ul>
<div class="float-clear"></div>
<div id="formContainer">
	<?php if($formErrors != null) { ?>
		<div class="errorBox">
			Neįvesti arba neteisingai įvesti šie laukai:
			<?php 
				echo $formErrors;
			?>
		</div>
	<?php } ?>
	<form action="" method="post">
		<fieldset>
			<legend>Personalo informacija</legend>
			<p>
				<label class="field" for="personalo_dydis">Darbuotojų skaičius<?php echo in_array('personalo_dydis', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="personalo_dydis" name="personalo_dydis" class="textbox-150" value="<?php echo isset($fields['personalo_dydis']) ? $fields['personalo_dydis'] : ''; ?>">
			</p>
			<p>
				<label class="field" for="vadybininko_vardas">Vadybininko vardas<?php echo in_array('vadybininko_vardas', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="vadybininko_vardas" name="vadybininko_vardas" class="textbox-150" value="<?php echo isset($fields['vadybininko_vardas']) ? $fields['vadybininko_vardas'] : ''; ?>">
			</p>
			<p>
				<label class="field" for="vadybininko_pavarde">Vadybininko pavardė<?php echo in_array('vadybininko_pavarde', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="vadybininko_pavarde" name="vadybininko_pavarde" class="textbox-150" value="<?php echo isset($fields['vadybininko_pavarde']) ? $fields['vadybininko_pavarde'] : ''; ?>">
			</p>
			
			<p>
				<?php if(!isset($fields['editing'])) { ?>
					<label class="field" for="id_Personalas">Personalo ID<?php echo in_array('id_Personalas', $required) ? '<span> *</span>' : ''; ?></label>
					<input type="text" id="id_Personalas" name="id_Personalas" class="textbox-70" value="<?php echo isset($fields['id_Personalas']) ? $fields['id_Personalas'] : ''; ?>">
					<?php if(key_exists('id_Personalas', $maxLengths)) echo "<span class='max-len'>(iki {$maxLengths['id_Personalas']} simb.)</span>"; ?>
				<?php } else { ?>
						<label class="field" for="id_Personalas">Personalo ID</label>
						<span class="input-value"><?php echo $fields['id_Personalas']; ?></span>
						<input type="hidden" name="editing" value="1" />
						<input type="hidden" name="id_Personalas" value="<?php echo $fields['id_Personalas']; ?>" />
				<?php } ?>
			</p>
			
			<p>
				<label class="field" for="brand">Boulingo Klubas<?php echo in_array('fk_Boulingo_klubasid_Boulingo_klubas', $required) ? '<span> *</span>' : ''; ?></label>
				<select id="brand" name="fk_Boulingo_klubasid_Boulingo_klubas">
					<option value="-1">--------</option>
					<?php
						// išrenkame visas ---
						$brands = $brandsObj->getBoulingoKlubasList();
						foreach($brands as $key => $val) {
							$selected = "";
							if(isset($fields['fk_Boulingo_klubasid_Boulingo_klubas']) && $fields['fk_Boulingo_klubasid_Boulingo_klubas'] == $val['id_Boulingo_klubas']) {
								$selected = " selected='selected'";
							}
							echo "<option{$selected} value='{$val['id_Boulingo_klubas']}'>{$val['pavadinimas']}</option>";
						}
					?>
				</select>
			</p>

			
		</fieldset>
		<p class="required-note">* pažymėtus laukus užpildyti privaloma</p>
		<p>
			<input type="submit" class="submit" name="submit" value="Išsaugoti">
		</p>
	</form>
</div>